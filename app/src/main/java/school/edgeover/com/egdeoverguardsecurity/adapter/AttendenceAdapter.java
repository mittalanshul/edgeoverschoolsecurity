package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnAttendenceListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.StaffDetails;
import school.edgeover.com.egdeoverguardsecurity.widget.CircularImageView;

/**
 * Created by anshul on 24/03/18.
 */

public class AttendenceAdapter extends RecyclerView.Adapter<AttendenceAdapter.MyViewHolder> {


    private ArrayList<SchoolStaff> staffDetailsArrayList;
    private Context mContext;
    private StaffDetails selectedStaff;

    public OnAttendenceListClickedCallback getOnAttendenceListClickedCallback() {
        return onAttendenceListClickedCallback;
    }

    public void setOnAttendenceListClickedCallback(OnAttendenceListClickedCallback onAttendenceListClickedCallback) {
        this.onAttendenceListClickedCallback = onAttendenceListClickedCallback;
    }

    private OnAttendenceListClickedCallback onAttendenceListClickedCallback;


    public ArrayList<SchoolStaff> getStaffDetailsArrayList() {
        return staffDetailsArrayList;
    }

    public void setStaffDetailsArrayList(ArrayList<SchoolStaff> staffDetailsArrayList) {
        this.staffDetailsArrayList = staffDetailsArrayList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView name;
        public CircularImageView circularImageView;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.rel_domestic);
            name = (TextView) view.findViewById(R.id.text_staff_name);
            circularImageView = (CircularImageView) view.findViewById(R.id.domestic_pic);
        }
    }

    public AttendenceAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public AttendenceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendence, parent, false);

        return new AttendenceAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final AttendenceAdapter.MyViewHolder holder, int position) {
        final SchoolStaff schoolStaff = staffDetailsArrayList.get(position);
        holder.name.setText(schoolStaff.getName());
        holder.relativeLayout.setTag(schoolStaff);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolStaff schoolStaff1 = (SchoolStaff) view.getTag();
                if(schoolStaff1 != null){
                    onAttendenceListClickedCallback.onAttendenceClicked(schoolStaff1);
                }
            }
        });
        String url = Constants.MEDIA_BASE_URL + schoolStaff.getImage();
        holder.circularImageView.setImageDrawable(null);
        Picasso.with(mContext).load(url).placeholder(R.drawable.user_pic_def).error(R.drawable.user_pic_def)
                .into(holder.circularImageView);
    }

    public StaffDetails getSelectedStaff() {
        return selectedStaff;
    }

    public void setSelectedStaff(StaffDetails selectedStaff) {
        this.selectedStaff = selectedStaff;
    }

    @Override
    public int getItemCount() {
        return staffDetailsArrayList == null ? 0 : staffDetailsArrayList.size();
    }
}

