package school.edgeover.com.egdeoverguardsecurity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;

import io.realm.Realm;
import io.realm.Sort;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.adapter.ChildEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.adapter.StaffEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.fragment.ChildInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.ChildOutFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffOutFragment;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

public class ChildRegisterActivity extends BaseActivity {
    private TabLayout tabLayout;
    private SearchView mSearchView;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_entry_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.CHILD_REGISTER);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        ChildEntryPagerAdapter adapter = new ChildEntryPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        createTabViews();
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Realm.getDefaultInstance()
                        .where(SchoolStaff.class)
                        .notEqualTo("date", Utils.getCurrentDate())
                        .findAll().deleteAllFromRealm();

            }
        });


    }

    private void createTabViews() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabOne.setText("CHILD IN");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabTwo.setText("CHILD OUT");
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch= ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    if(!TextUtils.isEmpty(query)){
                        int index = pager.getCurrentItem();
                        ChildEntryPagerAdapter adapter = ((ChildEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            ChildInFragment fragment = (ChildInFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshStaffDataOnQuery(query);
                            }
                        }else if(index == 1){
                            ChildOutFragment fragment = (ChildOutFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshVisitorsDataOnQuery(query);
                            }
                        }
                    }else{
                        int index = pager.getCurrentItem();
                        ChildEntryPagerAdapter adapter = ((ChildEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            ChildInFragment fragment = (ChildInFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }else if(index == 1){
                            ChildOutFragment fragment = (ChildOutFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }
                    }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }
}
