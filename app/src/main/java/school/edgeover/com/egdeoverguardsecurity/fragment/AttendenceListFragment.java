package school.edgeover.com.egdeoverguardsecurity.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Scanner;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.adapter.AttendenceAdapter;
import school.edgeover.com.egdeoverguardsecurity.adapter.SelectDomesticHelpAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnAttendenceListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;


public class AttendenceListFragment extends Fragment {


    public OnAttendenceListClickedCallback getOnAttendenceListClickedCallback() {
        return onAttendenceListClickedCallback;
    }

    public void setOnAttendenceListClickedCallback(OnAttendenceListClickedCallback onAttendenceListClickedCallback) {
        this.onAttendenceListClickedCallback = onAttendenceListClickedCallback;
    }

    private OnAttendenceListClickedCallback onAttendenceListClickedCallback;
    private RecyclerView recyclerView;
    private AttendenceAdapter attendenceAdapter;
    private View mView;
    private ArrayList<SchoolStaff> schoolStaffArrayList;


    public ArrayList<SchoolStaff> getSchoolStaffArrayList() {
        return schoolStaffArrayList;
    }

    public void setSchoolStaffArrayList(ArrayList<SchoolStaff> schoolStaffArrayList) {
        this.schoolStaffArrayList = schoolStaffArrayList;
    }



    public AttendenceListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setRecyclerViewData();

    }

    private void initViews(){
        recyclerView = mView.findViewById(R.id.rc);
    }

    private void setRecyclerViewData(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        attendenceAdapter = new AttendenceAdapter(getActivity());
        attendenceAdapter.setOnAttendenceListClickedCallback(onAttendenceListClickedCallback);
        attendenceAdapter.setStaffDetailsArrayList(schoolStaffArrayList);
        recyclerView.setAdapter(attendenceAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_attendence_list, container, false);
        return mView;
    }

    public void loadDefaultData(){
        if(schoolStaffArrayList != null && schoolStaffArrayList.size() >0){
            if(attendenceAdapter != null){
                recyclerView.setVisibility(View.VISIBLE);
                attendenceAdapter.setStaffDetailsArrayList(schoolStaffArrayList);
                attendenceAdapter.notifyDataSetChanged();
            }
        } else{
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void refreshDataOnSearch(String name){
        ArrayList<SchoolStaff> queriedList = queriedStaffList(name);
        if(queriedList != null && queriedList.size() >0){
            if(attendenceAdapter != null){
                recyclerView.setVisibility(View.VISIBLE);
                attendenceAdapter.setStaffDetailsArrayList(queriedList);
                attendenceAdapter.notifyDataSetChanged();
            }
        } else{
            recyclerView.setVisibility(View.GONE);
        }
    }

    private ArrayList<SchoolStaff> queriedStaffList(String query){
        ArrayList<SchoolStaff> schoolStaffs = new ArrayList<>();
        for(SchoolStaff schoolStaff : schoolStaffArrayList){
            if(schoolStaff.getName().toLowerCase().contains(query.toLowerCase())){
                schoolStaffs.add(schoolStaff);
            }
        }
        return schoolStaffs;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
