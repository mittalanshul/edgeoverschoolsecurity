package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.RealmResults;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnStaffInteractionCallback;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 24/03/18.
 */

public class ChildEntryListAdapter extends RecyclerView.Adapter<ChildEntryListAdapter.MyViewHolder> {

    private RealmResults<SchoolStaff> staffRealmResults;
    private OnStaffInteractionCallback onStaffInteractionCallback;

    public RealmResults<SchoolStaff> getStaffRealmResults() {
        return staffRealmResults;
    }

    public void setStaffRealmResults(RealmResults<SchoolStaff> staffRealmResults) {
        this.staffRealmResults = staffRealmResults;
    }

    public OnStaffInteractionCallback getOnStaffInteractionCallback() {
        return onStaffInteractionCallback;
    }

    public void setOnStaffInteractionCallback(OnStaffInteractionCallback onStaffInteractionCallback) {
        this.onStaffInteractionCallback = onStaffInteractionCallback;
    }

    public ChildEntryListAdapter(RealmResults<SchoolStaff> paramStaffs){
        staffRealmResults = paramStaffs;
    }

    @Override
    public ChildEntryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.staff_list_item, parent, false);
        return new ChildEntryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChildEntryListAdapter.MyViewHolder holder, int position) {
        final SchoolStaff staff = staffRealmResults.get(position);
        holder.staffName.setText(staff.getName());
        holder.staffNumber.setText(staff.getPhone());
        holder.staffType.setText(staff.getDesignation());
        holder.relativeLayout.setTag(staff);
        holder.staffExit.setTag(staff);
        if(staff.isExited()){
            holder.staffExit.setVisibility(View.GONE);
        }else{
            holder.staffExit.setVisibility(View.VISIBLE);
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolStaff staffClicked = (SchoolStaff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffClicked(staffClicked);
                }
            }
        });
        holder.staffExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolStaff staffClicked = (SchoolStaff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffExited(staffClicked);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return staffRealmResults.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView staffName;
        public TextView staffNumber;
        public TextView staffType;
        public TextView staffExit;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            staffName = (TextView) view.findViewById(R.id.text_staff_name);
            staffNumber = (TextView) view.findViewById(R.id.text_staff_number);
            staffType = (TextView) view.findViewById(R.id.text_type);
            staffExit = (TextView) view.findViewById(R.id.text_exit_staff);
        }
    }

}


