package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnCallListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.model.CallingList;
import school.edgeover.com.egdeoverguardsecurity.widget.CircularImageView;

/**
 * Created by anshul on 24/04/18.
 */

public class CallListAdapter extends RecyclerView.Adapter<CallListAdapter.MyViewHolder> {

    private ArrayList<CallingList> callingListArrayList;
    private Context mContext;
    private OnCallListClickedCallback onCallListClickedCallback;

    public ArrayList<CallingList> getCallingListArrayList() {
        return callingListArrayList;
    }

    public void setCallingListArrayList(ArrayList<CallingList> callingListArrayList) {
        this.callingListArrayList = callingListArrayList;
    }


    public OnCallListClickedCallback getOnCallListClickedCallback() {
        return onCallListClickedCallback;
    }

    public void setOnCallListClickedCallback(OnCallListClickedCallback onCallListClickedCallback) {
        this.onCallListClickedCallback = onCallListClickedCallback;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView name;
        public CircularImageView circularImageView;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.rel_domestic);
            name = (TextView) view.findViewById(R.id.text_staff_name);
            circularImageView = (CircularImageView) view.findViewById(R.id.domestic_pic);
        }
    }

    public CallListAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public CallListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendence, parent, false);

        return new CallListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final CallListAdapter.MyViewHolder holder, int position) {
        final CallingList callingList = callingListArrayList.get(position);
        holder.name.setText(callingList.getName());
        holder.relativeLayout.setTag(callingList.getNumber());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String callingNumber  = (String) view.getTag();
                if(!TextUtils.isEmpty(callingNumber)){
                    onCallListClickedCallback.onCallMake(callingNumber);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return callingListArrayList == null ? 0 : callingListArrayList.size();
    }
}


