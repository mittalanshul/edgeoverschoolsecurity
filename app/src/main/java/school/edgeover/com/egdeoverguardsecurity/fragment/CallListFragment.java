package school.edgeover.com.egdeoverguardsecurity.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.adapter.CallListAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnCallListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.model.CallingList;

/**
 * Created by anshul on 24/04/18.
 */

public class CallListFragment extends Fragment {

    public OnCallListClickedCallback getOnCallListClickedCallback() {
        return onCallListClickedCallback;
    }

    public void setOnCallListClickedCallback(OnCallListClickedCallback onCallListClickedCallback) {
        this.onCallListClickedCallback = onCallListClickedCallback;
    }

    private OnCallListClickedCallback onCallListClickedCallback;
    private RecyclerView recyclerView;
    private CallListAdapter callListAdapter;
    private View mView;
    private ArrayList<CallingList> callingListArrayList;

    public ArrayList<CallingList> getCallingListArrayList() {
        return callingListArrayList;
    }

    public void setCallingListArrayList(ArrayList<CallingList> callingListArrayList) {
        this.callingListArrayList = callingListArrayList;
    }

    public CallListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setRecyclerViewData();

    }

    private void initViews(){
        recyclerView = mView.findViewById(R.id.rc);
    }

    private void setRecyclerViewData(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        callListAdapter = new CallListAdapter(getActivity());
        callListAdapter.setOnCallListClickedCallback(onCallListClickedCallback);
        callListAdapter.setCallingListArrayList(getCallingArrayList());
        recyclerView.setAdapter(callListAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private ArrayList<CallingList> getCallingArrayList(){
        callingListArrayList = new ArrayList<>();
        CallingList callingList = new CallingList();
        callingList.setName("Pre School");
        callingList.setNumber("08527274695");
        callingListArrayList.add(callingList);

        callingList = new CallingList();
        callingList.setName("Day Care");
        callingList.setNumber("09871099537");
        callingListArrayList.add(callingList);

        callingList = new CallingList();
        callingList.setName("Madhumita Sarkar");
        callingList.setNumber("09999288842");
        callingListArrayList.add(callingList);

        callingList = new CallingList();
        callingList.setName("Romilla");
        callingList.setNumber("09818360006");
        callingListArrayList.add(callingList);

        callingList = new CallingList();
        callingList.setName("Isha");
        callingList.setNumber("08901338618");
        callingListArrayList.add(callingList);
        return callingListArrayList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_attendence_list, container, false);
        return mView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

