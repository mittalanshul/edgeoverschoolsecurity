package school.edgeover.com.egdeoverguardsecurity.callbacks;

import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 25/03/18.
 */

public interface OnVisitorSelectedCallback {
    public void onStaffSelectedToVisit(SchoolStaff schoolStaff);
}
