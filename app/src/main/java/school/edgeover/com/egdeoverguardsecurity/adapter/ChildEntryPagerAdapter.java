package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.HashMap;

import school.edgeover.com.egdeoverguardsecurity.fragment.ChildInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.ChildOutFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffOutFragment;

/**
 * Created by anshul on 24/03/18.
 */

public class ChildEntryPagerAdapter extends FragmentStatePagerAdapter {
    private ChildInFragment childInFragment;
    private ChildOutFragment childOutFragment;
    private HashMap<Integer,Fragment> mPageReferenceMap = new HashMap<>();
    public ChildEntryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }

    public Fragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                childInFragment = new ChildInFragment();
                fragment = childInFragment;
                fragment.setRetainInstance(true);
                break;
            case 1:
                childOutFragment = new ChildOutFragment();
                fragment = childOutFragment;
                fragment.setRetainInstance(true);
                break;
        }
        mPageReferenceMap.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}

