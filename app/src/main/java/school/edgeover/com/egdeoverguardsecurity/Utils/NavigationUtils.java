package school.edgeover.com.egdeoverguardsecurity.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import school.edgeover.com.egdeoverguardsecurity.AttendenceActivity;
import school.edgeover.com.egdeoverguardsecurity.ChildRegisterActivity;
import school.edgeover.com.egdeoverguardsecurity.KnowYourSocietyLogin;
import school.edgeover.com.egdeoverguardsecurity.RWALoginActivity;
import school.edgeover.com.egdeoverguardsecurity.SocietyFacilitiesActivity;
import school.edgeover.com.egdeoverguardsecurity.StaffAttendenceActivity;
import school.edgeover.com.egdeoverguardsecurity.StaffEntryRegister;
import school.edgeover.com.egdeoverguardsecurity.VisitorEntryActivity;
import school.edgeover.com.egdeoverguardsecurity.VisitorExitActionActivity;
import school.edgeover.com.egdeoverguardsecurity.VisitorRegisterActivity;
import school.edgeover.com.egdeoverguardsecurity.adapter.ChildEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.AllSchoolData;
import school.edgeover.com.egdeoverguardsecurity.model.AllSocietyData;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;


/**
 * Created by 201101101 on 10/16/2017.
 */

public class NavigationUtils {

    public static void navigateToSocietyFacilities(Context context){
        Intent intent = new Intent(context, SocietyFacilitiesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void navigateToLogin(Context context,String mode){
        Intent intent = new Intent(context, RWALoginActivity.class);
        intent.putExtra("mode",mode);
        ((Activity)context).startActivityForResult(intent,1);
    }

    public static void navigateToModeLogin(Context context){
        Intent intent = new Intent(context, KnowYourSocietyLogin.class);
        context.startActivity(intent);
    }


    public static void navigateToVisitorEntry(Context context , AllSchoolData allSchoolData){
        Intent intent = new Intent(context, VisitorEntryActivity.class);
        intent.putExtra("all_data",allSchoolData);
        context.startActivity(intent);
    }


    public static void navigateToVisitorEntryRegister(Context context){
        Intent intent = new Intent(context, VisitorRegisterActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToStaffEntryRegister(Context context){
        Intent intent = new Intent(context, StaffEntryRegister.class);
        context.startActivity(intent);
    }

    public static void navigateToChildEntryRegister(Context context){
        Intent intent = new Intent(context, ChildRegisterActivity.class);
        context.startActivity(intent);
    }

    public static void navigateToAttendence(Context context , AllSchoolData allSchoolData){
        Intent intent = new Intent(context, AttendenceActivity.class);
        intent.putExtra("all_data",allSchoolData);
        context.startActivity(intent);
    }

    public static void navigateToStaffAttendence(Context context , AllSchoolData allSchoolData){
        Intent intent = new Intent(context, StaffAttendenceActivity.class);
        intent.putExtra("all_data",allSchoolData);
        context.startActivity(intent);
    }

    public static void navigateToVisitorAction(Context context,int requestCode,String visName , String visNumber , String purpose , String personMeet){
        Intent intent = new Intent(context, VisitorExitActionActivity.class);
        intent.putExtra("data1",visName);
        intent.putExtra("data2",visNumber);
        intent.putExtra("data3",purpose);
        intent.putExtra("data4",personMeet);
        ((Activity)context).startActivityForResult(intent,requestCode);
    }

    /*public static void navigateToResidentVisitorHistory(Context context){
        Intent intent = new Intent(context, ResidentVisitorHistory.class);
        context.startActivity(intent);
    }



    public static void navigateToResidentEntry(Context context , AllSocietyData allSocietyData){
        Intent intent = new Intent(context, ResidentEntryActivity.class);
        intent.putExtra("all_data",allSocietyData);
        context.startActivity(intent);
    }

    public static void navigateToResidentProfile(Context context, Resident resident){
        Intent intent = new Intent(context, ResidentProfileActivity.class);
        intent.putExtra("resident",resident);
        context.startActivity(intent);
    }

    public static void navigateToPanicAlert(Context context, String data,String message){
        Intent intent = new Intent(context, PanicAlertActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.NOTIFICATION_DATA,data);
        intent.putExtra("message",message);
        context.startActivity(intent);
    }*/


    public static void navigateToLogout(Context context){
        Utils.clearDataOnLogout(context);
    }
}
