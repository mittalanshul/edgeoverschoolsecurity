package school.edgeover.com.egdeoverguardsecurity.parser;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.AllSocietyData;
import school.edgeover.com.egdeoverguardsecurity.model.Resident;
import school.edgeover.com.egdeoverguardsecurity.model.StaffDetails;

/**
 * Created by 201101101 on 10/28/2017.
 */

public class AllSocietyDataParser {

    public static AllSocietyData parseAllSocietyData(String response){
        AllSocietyData allSocietyData = new AllSocietyData();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject dataObject = jsonObject.optJSONObject(Keys.DATA);
                if(dataObject != null){
                    JSONArray serviceJsonArray = dataObject.optJSONArray(Keys.SERVICE);
                        if(serviceJsonArray != null && serviceJsonArray.length()>0){
                            ArrayList<StaffDetails> maidArrayList = new ArrayList<>();
                            ArrayList<StaffDetails> carCLeanerArrayList = new ArrayList<>();
                            for (int j = 0; j < serviceJsonArray.length(); j++) {
                                JSONObject serviceObject = serviceJsonArray.optJSONObject(j);
                                if(serviceObject != null){
                                    StaffDetails staffDetails = new StaffDetails();
                                    staffDetails.setStaffName(serviceObject.optString(Keys.NAME));
                                    staffDetails.setStaffId(serviceObject.optString(Keys.ID));
                                    staffDetails.setStaffNumber(serviceObject.optString(Keys.PHONE));
                                    staffDetails.setStaffType(String.valueOf(serviceObject.optString(Keys.TYPE)));
                                    staffDetails.setImage(String.valueOf(serviceObject.optString(Keys.IMAGE)));

                                    if(staffDetails.getStaffType().equalsIgnoreCase(Constants.MAID)){
                                        maidArrayList.add(staffDetails);
                                    }else if(staffDetails.getStaffType().equalsIgnoreCase(Constants.CAR_CLEANER)){
                                        carCLeanerArrayList.add(staffDetails);
                                    }
                                }
                            }
                            allSocietyData.setMaidArrayList(maidArrayList);
                            allSocietyData.setCarCleanerList(carCLeanerArrayList);
                        }

                    JSONArray staffJsonArray = dataObject.optJSONArray(Keys.STAFF);
                    if(staffJsonArray != null && staffJsonArray.length()>0){
                        ArrayList<StaffDetails> guardArrayList = new ArrayList<>();
                        ArrayList<StaffDetails> officeArrayList = new ArrayList<>();
                        ArrayList<StaffDetails> otherArrayList = new ArrayList<>();
                        for (int j = 0; j < staffJsonArray.length(); j++) {
                            JSONObject staffObject = staffJsonArray.optJSONObject(j);
                            if(staffObject != null){
                                StaffDetails staffDetails = new StaffDetails();
                                staffDetails.setStaffName(staffObject.optString(Keys.NAME));
                                staffDetails.setStaffId(staffObject.optString(Keys.ID));
                                staffDetails.setStaffNumber(staffObject.optString(Keys.PHONE));
                                staffDetails.setStaffType(String.valueOf(staffObject.optString(Keys.TYPE)));
                                staffDetails.setImage(String.valueOf(staffObject.optString(Keys.IMAGE)));

                                if(staffDetails.getStaffType().equalsIgnoreCase(Constants.GUARDS)){
                                    guardArrayList.add(staffDetails);
                                }else if(staffDetails.getStaffType().equalsIgnoreCase(Constants.OFFICE)){
                                    officeArrayList.add(staffDetails);
                                }else if(staffDetails.getStaffType().equalsIgnoreCase(Constants.OTHER)){
                                    otherArrayList.add(staffDetails);
                                }
                            }
                        }
                        allSocietyData.setGuardArrayList(guardArrayList);
                        allSocietyData.setOfficeArrayList(officeArrayList);
                        allSocietyData.setOthersArrayList(otherArrayList);
                    }

                    JSONArray residentArray = dataObject.optJSONArray(Keys.RESIDENT);
                    if(residentArray != null && residentArray.length() >0){
                        HashMap<String,Resident> residentHashMap = new HashMap<>();
                        TreeSet<String> towerSet = new TreeSet<>();
                        TreeSet<String> towerflatSet = new TreeSet<>();
                        for(int i =0;i<residentArray.length();i++){
                            JSONObject residentObject = residentArray.optJSONObject(i);
                            if(residentObject != null){
                                Resident resident = parseResidentData(residentObject);
                                if(resident != null){
                                    if(!TextUtils.isEmpty(resident.getAddress1())){
                                        towerSet.add(resident.getAddress1());
                                        if(!TextUtils.isEmpty(resident.getAddress2())){
                                            towerflatSet.add(resident.getAddress1() + " " + resident.getAddress2());
                                            residentHashMap.put(resident.getAddress1() + " " + resident.getAddress2(),resident);
                                        }
                                    }
                                }
                            }
                        }
                        allSocietyData.setResidentHashMap(residentHashMap);
                        allSocietyData.setTowerSet(towerSet);
                        allSocietyData.setFlatSet(towerflatSet);
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allSocietyData;
    }

    public static Resident parseResidentData(JSONObject jsonObject){
        Resident resident = new Resident();
        resident.setAuthority(jsonObject.optString(Keys.AUTHORITY));
        resident.setAddress1(jsonObject.optString(Keys.ADDRESS1));
        resident.setAddress2(jsonObject.optString(Keys.ADDRESS2));
        resident.setName(jsonObject.optString(Keys.NAME));
        resident.setPhone1(jsonObject.optString(Keys.PHONE1));
        resident.setPhone2(jsonObject.optString(Keys.PHONE2));
        resident.setJointOwner(jsonObject.optBoolean(Keys.IS_JOINT_OWNER));
        resident.setJointName(jsonObject.optString(Keys.JOINT_NAME));
        resident.setIsRented(jsonObject.optBoolean(Keys.IS_RENTED));
        resident.setTenantName(jsonObject.optString(Keys.TENANT_NAME));
        resident.setTenantNumber(jsonObject.optString(Keys.TENANT_NUMBER));
        resident.setId(jsonObject.optString(Keys.ID));
        return resident;
    }
}
