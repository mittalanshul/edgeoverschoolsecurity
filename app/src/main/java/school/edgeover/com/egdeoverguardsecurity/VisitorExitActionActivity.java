package school.edgeover.com.egdeoverguardsecurity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import school.edgeover.com.egdeoverguardsecurity.constants.Constants;

public class VisitorExitActionActivity extends BaseActivity implements View.OnClickListener{

    private TextView textDeny;
    private TextView textApprove;
    private String data;
    private TextView textVisName;
    private TextView textVisNum;
    private TextView textPurpose;
    private TextView textPerson;


    private String visName;
    private String visNumber;
    private String purpose;
    private String personMeet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_visitor_exit_action);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initViews();
        initClickListener();

        if(getIntent() != null && getIntent().getExtras() != null){
            visName = getIntent().getStringExtra("data1");
            visNumber = getIntent().getStringExtra("data2");
            purpose = getIntent().getStringExtra("data3");
            personMeet = getIntent().getStringExtra("data4");
                setData();
        }
    }

    private void setData(){
        textVisName.setText(visName);
        textVisNum.setText(visNumber);
        textPurpose.setText(purpose);
        textPerson.setText(personMeet);
    }

    private void initViews(){
        textDeny = (TextView)findViewById(R.id.btn_deny);
        textApprove = (TextView)findViewById(R.id.btn_approve);
        textVisName = (TextView)findViewById(R.id.visitor_name);
        textVisNum = (TextView)findViewById(R.id.visitor_number);
        textPurpose = (TextView)findViewById(R.id.purpose);
        textPerson = (TextView)findViewById(R.id.person_meet);

    }

    private void initClickListener(){
        textDeny.setOnClickListener(this);
        textApprove.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_deny:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btn_approve:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }
}
