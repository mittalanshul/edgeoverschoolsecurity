/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.callbacks;


import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.Staff;

/**
 * Created by 201101101 on 11/4/2017.
 */

public interface OnStaffInteractionCallback {
    public void onStaffExited(SchoolStaff staff);
    public void onStaffClicked(SchoolStaff staff);
}
