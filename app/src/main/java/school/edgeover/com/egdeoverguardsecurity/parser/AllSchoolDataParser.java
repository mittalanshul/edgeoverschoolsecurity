package school.edgeover.com.egdeoverguardsecurity.parser;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Logger;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.model.AllSchoolData;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 24/03/18.
 */

public class AllSchoolDataParser  {

    public static AllSchoolData getAllSchoolData(String response){
        AllSchoolData allSchoolData = new AllSchoolData();
        try {
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject(Keys.DATA);
            if(jsonObject != null){
                JSONArray jsonArrayTeaher = jsonObject.optJSONArray(Keys.TEACHER);
                if(jsonArrayTeaher != null && jsonArrayTeaher.length()>0){
                    ArrayList<SchoolStaff> teacherList = new ArrayList<>();
                    for(int i =0 ; i < jsonArrayTeaher.length();i++){
                        JSONObject jsonObject1 = jsonArrayTeaher.optJSONObject(i);
                        if(jsonObject1 != null){
                            SchoolStaff schoolStaff = parseSchoolStaff(jsonObject1);
                            if(schoolStaff != null){
                                teacherList.add(schoolStaff);
                            }
                        }
                    }
                    allSchoolData.setTeacherArrayList(teacherList);
                }

                JSONArray jsonArrayStaff = jsonObject.optJSONArray(Keys.STAFF);
                if(jsonArrayStaff != null && jsonArrayStaff.length()>0){
                    ArrayList<SchoolStaff> staffList = new ArrayList<>();
                    ArrayList<SchoolStaff> domesticStaffList = new ArrayList<>();
                    for(int i =0 ; i < jsonArrayStaff.length();i++){
                        JSONObject jsonObject1 = jsonArrayStaff.optJSONObject(i);
                        if(jsonObject1 != null){
                            SchoolStaff schoolStaff = parseSchoolStaff(jsonObject1);
                            if(schoolStaff != null){
                                if(!TextUtils.isEmpty(schoolStaff.getRole())
                                        && schoolStaff.getRole().toLowerCase().
                                        equalsIgnoreCase("DOMESTIC STAFF".toLowerCase())){
                                    domesticStaffList.add(schoolStaff);
                                }else{
                                    staffList.add(schoolStaff);
                                }
                            }
                        }
                    }
                    if(staffList != null && staffList.size()>0){
                        allSchoolData.setStaffArrayList(staffList);
                    }
                    if(domesticStaffList != null && domesticStaffList.size() >0){
                        allSchoolData.setDomesticStaffArrayList(domesticStaffList);
                    }

                }

                JSONArray jsonArrayChild = jsonObject.optJSONArray(Keys.CHILD);
                if(jsonArrayChild != null && jsonArrayChild.length()>0){
                    ArrayList<SchoolStaff> childList = new ArrayList<>();
                    for(int i =0 ; i < jsonArrayChild.length();i++){
                        JSONObject jsonObject1 = jsonArrayChild.optJSONObject(i);
                        if(jsonObject1 != null){
                            SchoolStaff schoolStaff = parseSchoolStaff(jsonObject1);
                            if(schoolStaff != null){
                                schoolStaff.setChild(true);
                                if(!TextUtils.isEmpty(schoolStaff.getClassRoom()) &&
                                        schoolStaff.getClassRoom().toLowerCase().equalsIgnoreCase("daycare")){
                                    childList.add(schoolStaff);
                                }
                            }
                        }
                    }
                    allSchoolData.setChildArrayLst(childList);
                }

                SchoolStaff schoolStaff = new SchoolStaff();
                schoolStaff.setName(jsonObject.optString(Keys.NAME));
                schoolStaff.setPhone(jsonObject.optString(Keys.PHONE1));
                schoolStaff.setAddress(jsonObject.optString(Keys.ADDRESS1));
                schoolStaff.setEmail(jsonObject.optString(Keys.EMAIL));
                allSchoolData.setAdmissionStaff(schoolStaff);



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allSchoolData;
    }

    private static SchoolStaff parseSchoolStaff(JSONObject jsonObject){
        SchoolStaff schoolStaff = new SchoolStaff();
        schoolStaff.setName(jsonObject.optString(Keys.NAME));
        schoolStaff.setPhone(jsonObject.optString(Keys.PHONE));
        schoolStaff.setEmail(jsonObject.optString(Keys.EMAIL));
        schoolStaff.setAddress(jsonObject.optString(Keys.ADDRESS));
        schoolStaff.setFatherName(jsonObject.optString(Keys.FATHERNAME));
        schoolStaff.setMotherName(jsonObject.optString(Keys.MOTHERNAME));
        schoolStaff.setClassRoom(jsonObject.optString(Keys.CLASSROOM));
        schoolStaff.setId(jsonObject.optString(Keys.ID));
        schoolStaff.setImage(jsonObject.optString(Keys.IMAGE));
        schoolStaff.setDesignation(jsonObject.optString(Keys.DESIGNATION));
        schoolStaff.setRole(jsonObject.optString(Keys.ROLE));
        return schoolStaff;
    }
}
