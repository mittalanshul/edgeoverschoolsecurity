package school.edgeover.com.egdeoverguardsecurity.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 07/04/18.
 */

public class ChildInfoDialog extends DialogFragment {

    private TextView textStaffName;
    private TextView textStaffNumber;
    private TextView textStaffType;
    private TextView textStaffId;
    private TextView textAttendenceDate;
    private TextView textAttendenceTime;
    private View rootView;
    private SchoolStaff staff;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.child_info_dialog, container, false);
        getDialog().setTitle("Staff Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
        textStaffName = (TextView)rootView.findViewById(R.id.text_staff_name);
        textStaffNumber = (TextView)rootView.findViewById(R.id.text_staff_number);
        textStaffId = (TextView)rootView.findViewById(R.id.text_staff_id);
        textAttendenceDate = (TextView)rootView.findViewById(R.id.text_staff_attendence_date);
        textAttendenceTime = (TextView)rootView.findViewById(R.id.text_staff_attendence_time);
    }

    public void setStaffData(SchoolStaff paramStaff){
        staff = paramStaff ;
    }

    private void setData(){
        textStaffName.setText(staff.getName());
        textStaffNumber.setText(staff.getPhone());
        textStaffId.setText(staff.getId());
        textAttendenceDate.setText(staff.getDate() + " / " + staff.getTime());
        if(!TextUtils.isEmpty(staff.getDateExited())){
            textAttendenceTime.setText(staff.getDateExited() + " / " + staff.getTimeExited());
        }else{
            textAttendenceTime.setText("Not Exited Yet");
        }

    }
}


