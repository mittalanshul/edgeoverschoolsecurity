package school.edgeover.com.egdeoverguardsecurity.parser;


import org.json.JSONException;
import org.json.JSONObject;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.model.BaseModel;

/**
 * Created by 201101101 on 11/24/2017.
 */

public class BaseModelParser {

    public static BaseModel parseBaseModel(String response){
        BaseModel baseModel = new BaseModel();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                baseModel.setStatus(jsonObject.optBoolean(Keys.STATUS));
                baseModel.setMessage(jsonObject.optString(Keys.MESSAGE));
                baseModel.setData(jsonObject.optString(Keys.DATA));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return baseModel;
    }
}
