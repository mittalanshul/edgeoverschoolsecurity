package school.edgeover.com.egdeoverguardsecurity.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Created by 201101101 on 11/5/2017.
 */

public class StaffDetails extends RealmObject implements Parcelable{
    private String staffId;
    private String staffName;
    private String staffType;
    private String staffNumber;
    private String image;
    private boolean isSelected;

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    private String idProof;

    protected StaffDetails(Parcel in) {
        staffId = in.readString();
        staffName = in.readString();
        staffType = in.readString();
        staffNumber = in.readString();
        isSelected = in.readByte() != 0;
        image = in.readString();
        idProof = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(staffId);
        dest.writeString(staffName);
        dest.writeString(staffType);
        dest.writeString(staffNumber);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(image);
        dest.writeString(idProof);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StaffDetails> CREATOR = new Creator<StaffDetails>() {
        @Override
        public StaffDetails createFromParcel(Parcel in) {
            return new StaffDetails(in);
        }

        @Override
        public StaffDetails[] newArray(int size) {
            return new StaffDetails[size];
        }
    };

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



    public StaffDetails(){

    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }


    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

