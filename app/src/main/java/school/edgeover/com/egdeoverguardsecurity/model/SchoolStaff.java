package school.edgeover.com.egdeoverguardsecurity.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Created by anshul on 24/03/18.
 */

public class SchoolStaff extends RealmObject implements Parcelable {

    private String name;
    private String phone;
    private String email;
    private String address;
    private String designation;
    private String role;
    private String id;
    private String image;
    private String fatherName;
    private String motherName;
    private String date;
    private String time;
    private String dateExited;
    private String timeExited;
    private boolean isExited;
    private String aid;

    protected SchoolStaff(Parcel in) {
        name = in.readString();
        phone = in.readString();
        email = in.readString();
        address = in.readString();
        designation = in.readString();
        role = in.readString();
        id = in.readString();
        image = in.readString();
        fatherName = in.readString();
        motherName = in.readString();
        date = in.readString();
        time = in.readString();
        dateExited = in.readString();
        timeExited = in.readString();
        isExited = in.readByte() != 0;
        aid = in.readString();
        isChild = in.readByte() != 0;
        classRoom = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(designation);
        dest.writeString(role);
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(fatherName);
        dest.writeString(motherName);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(dateExited);
        dest.writeString(timeExited);
        dest.writeByte((byte) (isExited ? 1 : 0));
        dest.writeString(aid);
        dest.writeByte((byte) (isChild ? 1 : 0));
        dest.writeString(classRoom);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SchoolStaff> CREATOR = new Creator<SchoolStaff>() {
        @Override
        public SchoolStaff createFromParcel(Parcel in) {
            return new SchoolStaff(in);
        }

        @Override
        public SchoolStaff[] newArray(int size) {
            return new SchoolStaff[size];
        }
    };

    public String getDateExited() {
        return dateExited;
    }

    public void setDateExited(String dateExited) {
        this.dateExited = dateExited;
    }

    public String getTimeExited() {
        return timeExited;
    }

    public void setTimeExited(String timeExited) {
        this.timeExited = timeExited;
    }


    public boolean isChild() {
        return isChild;
    }

    public void setChild(boolean child) {
        isChild = child;
    }

    private boolean isChild;

    public boolean isExited() {
        return isExited;
    }

    public void setExited(boolean exited) {
        isExited = exited;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    private String classRoom;

    public SchoolStaff(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }
}
