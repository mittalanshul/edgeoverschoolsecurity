/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;

import io.realm.Realm;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.adapter.StaffEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.adapter.VisitorEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.StaffOutFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.VisitorInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.VisitorOutFragment;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;


public class StaffEntryRegister extends BaseActivity {
    private TabLayout tabLayout;
    private SearchView mSearchView;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_entry_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.STAFF_REGISTER);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        StaffEntryPagerAdapter adapter = new StaffEntryPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        createTabViews();
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Realm.getDefaultInstance()
                        .where(SchoolStaff.class)
                        .notEqualTo("date", Utils.getCurrentDate())
                        .findAll().deleteAllFromRealm();

            }
        });

    }

    private void createTabViews() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabOne.setText("STAFF IN");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabTwo.setText("STAFF OUT");
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch= ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    if(!TextUtils.isEmpty(query)){
                        int index = pager.getCurrentItem();
                        StaffEntryPagerAdapter adapter = ((StaffEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            StaffInFragment fragment = (StaffInFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshStaffDataOnQuery(query);
                            }
                        }else if(index == 1){
                            StaffOutFragment fragment = (StaffOutFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.refreshVisitorsDataOnQuery(query);
                            }
                        }
                    }else{
                        int index = pager.getCurrentItem();
                        StaffEntryPagerAdapter adapter = ((StaffEntryPagerAdapter)pager.getAdapter());
                        if(index == 0){
                            StaffInFragment fragment = (StaffInFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }else if(index == 1){
                            StaffOutFragment fragment = (StaffOutFragment) adapter.getFragment(index);
                            if(fragment != null){
                                fragment.loadDefaultData();
                            }
                        }
                    }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }
}
