/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import school.edgeover.com.egdeoverguardsecurity.Utils.NavigationUtils;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;


public class SplashActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private ImageView mImageView;
    private TextView mTextView;

    Intent mServiceIntent;

    Context ctx;
    public Context getCtx() {
        return ctx;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        NavigationUtils.navigateToModeLogin(SplashActivity.this);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Utils.isUserLoggedIn(this)){
            NavigationUtils.navigateToSocietyFacilities(this);
            finish();
        }
        setContentView(R.layout.activity_splash);
        relativeLayout = (RelativeLayout)findViewById(R.id.root_splash);
        mImageView = (ImageView) findViewById(R.id.img_splash);
        mTextView = (TextView) findViewById(R.id.text_splash);
        fadeSplashOut();



    }

    private void fadeSplashOut() {
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        //relativeLayout.setAlpha(0f);
        relativeLayout.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        /*relativeLayout.animate()
                .alpha(1f)
                .setDuration(3000)
                .setListener(null);*/
        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mTextView.animate()
                .alpha(0f)
                .setDuration(3000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                       NavigationUtils.navigateToModeLogin(SplashActivity.this);
                        finish();

                    }
                });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

}
