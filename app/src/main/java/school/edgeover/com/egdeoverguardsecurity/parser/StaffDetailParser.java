package school.edgeover.com.egdeoverguardsecurity.parser;

import org.json.JSONException;
import org.json.JSONObject;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 27/04/18.
 */

public class StaffDetailParser {

    public static SchoolStaff parseSchoolStaffForAttendance(String response){
        SchoolStaff schoolStaff = new SchoolStaff();
        try {
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject(Keys.DATA);
            if(jsonObject != null){
                JSONObject jsonObjectEntry = jsonObject.optJSONObject(Keys.ENTRY);
                if(jsonObjectEntry != null){
                    schoolStaff.setAid(jsonObjectEntry.optString(Keys.ID));
                }
                JSONObject jsonObjectHost = jsonObject.optJSONObject(Keys.HOST);
                if(jsonObjectHost != null){
                    schoolStaff.setName(jsonObjectHost.optString(Keys.NAME));
                    schoolStaff.setPhone(jsonObjectHost.optString(Keys.PHONE));
                    schoolStaff.setEmail(jsonObjectHost.optString(Keys.EMAIL));
                    schoolStaff.setAddress(jsonObjectHost.optString(Keys.ADDRESS));
                    schoolStaff.setFatherName(jsonObject.optString(Keys.FATHERNAME));
                    schoolStaff.setMotherName(jsonObject.optString(Keys.MOTHERNAME));
                    schoolStaff.setImage(jsonObjectHost.optString(Keys.IMAGE));
                    schoolStaff.setDesignation(jsonObjectHost.optString(Keys.DESIGNATION));
                    schoolStaff.setRole(jsonObjectHost.optString(Keys.ROLE));
                    schoolStaff.setId(jsonObjectHost.optString(Keys.ID));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return schoolStaff;
    }
}
