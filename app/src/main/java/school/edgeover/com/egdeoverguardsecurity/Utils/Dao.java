package school.edgeover.com.egdeoverguardsecurity.Utils;


import school.edgeover.com.egdeoverguardsecurity.model.Token;

/**
 * Created by utsavjain on 24/02/18.
 */

public class Dao {
    private static final Dao ourInstance = new Dao();
    private static Token token;
    public static Dao getInstance() {
        return ourInstance;
    }

    private Dao() {
    }

    public Token getToken() {
        return this.token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
