/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.constants;


/**
 * Created by 201101101 on 10/16/2017.
 */

public class Constants {


    public static final String MAID = "Maid";
    public static final String OFFICE = "Office";
    public static final String OTHER = "Service";
    public static final String CAR_CLEANER = "Car Cleaner";
    public static final String ENQUIRE = "Enquire";
    public static final String ADMISSION = "New Admission";
    public static final String TEACHER = "Teacher";
    public static final String STAFF = "Office Staff";
    public static final String VENDOR = "Vendor";
    public static final String DOMESTIC_STAFF = "Domestic Staff";
    public static final String CHILD = "Child";
    public static final String GUARDS = "Guard";
    public static final String ATTENDENCE = "Day Care Child Attendance";
    public static final String CHILD_REGISTER = "Day Care Child Register";
    public static final String STAFF_ATTENDENCE = "Staff Attendance";

    public static final String VISITOR_ENTRY = "Create Visitor";
    public static final String VISITOR_REGISTER = "Visitor Register";
    public static final String STAFF_REGISTER = "Staff Register";
    public static final String PARENTS_REGISTER = "Parents Register";
    public static final String PARENTS_ENTRY = "Parents Entry";

    public static final String TYPE = "type";

    public static final String MODE_RWA = "mode_rwa";
    public static final String MODE_RESIDENT = "mode_resident";
    public static final String MODE_GUARD = "mode_guard";
    public static final int REQUEST_PERMISSIONS = 1045;

    public static String BASE_URL ="http://52.15.122.175:9000"; // //18.218.71.155
    public static String AUTHORITY_lOGIN ="/authority/user";
    public static String GUARD_lOGIN = "/login/entry";
    public static String GET_ALL_GUARD_DATA ="/entry";
    public static String CREATE_VISITOR_ENTRY_URL = "/entry/visitor?";
    public static String CREATE_RESIDENT_EXPECTED_VISITOR_ENTRY_URL = "/resident/visitor";
    public static String CREATE_STAFF_ENTRY_URL ="/entry/attendance";
    public static String EXIT_VISITOR ="/exit/visitor?";
    public static String RESIDENT_ALERT ="/resident/alert?type=fire";
    public static final String ENTRY_ALERT="/entry/alert";
    public static String ENTRY_OUT = "/entry/out";
    public static String ENTRY_IN = "/entry/in";

    public static String AUTHORIZATION_HEADER ="Authorization";

    public static int LOGIN_REQUEST_CODE = 77;

    public static String AD_COMPLAINT = "r-complaint";
    public static String AD_ALERT = "g-alert";
    public static String AD_VISITOR = "r-visitor-action";
    public static String AD_CHILD_SAFETY = "r-child-safety";
    public static String AD_CHILD_ACTION = "g-child-action";
    public static String AD_VISITOR_ACTION = "r-visitor";
    public static String AD_GUARD_VISITOR_ACTION = "g-visitor-action";
    public static String AD_MAINTAINENCE= "r-maintainance";
    public static String AD_EVENT= "r-event";
    public static String AD_NOTICE= "r-notice";
    public static String AD_SERVICE= "r-service";
    public static String A_COMPLAINT= "a-complaint";
    public static String R_ALERT = "r-alert";

    public static final int MAX_EMERGENCY_CONACTS = 2;
    public static final String INTENT_ACTION_BROADCAST = "intent_action_broadcast";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String NOTIFICATION_MESSAGE = "notification_message";
    public static final String NOTIFICATION_DATA = "notification_data";
    public static final String SOLVED = "solved";
    public static final String REOPEN = "reopen";
    public static final String OPEN = "Pending";
    public static final String MEDIA_BASE_URL = "https://s3.ap-south-1.amazonaws.com/www.edgeover.in/";

}
