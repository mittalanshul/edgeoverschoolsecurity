/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.RealmResults;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnStaffInteractionCallback;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.Staff;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffEntryListAdapter extends RecyclerView.Adapter<StaffEntryListAdapter.MyViewHolder> {

    private RealmResults<SchoolStaff> staffRealmResults;
    private OnStaffInteractionCallback onStaffInteractionCallback;

    public RealmResults<SchoolStaff> getStaffRealmResults() {
        return staffRealmResults;
    }

    public void setStaffRealmResults(RealmResults<SchoolStaff> staffRealmResults) {
        this.staffRealmResults = staffRealmResults;
    }

    public OnStaffInteractionCallback getOnStaffInteractionCallback() {
        return onStaffInteractionCallback;
    }

    public void setOnStaffInteractionCallback(OnStaffInteractionCallback onStaffInteractionCallback) {
        this.onStaffInteractionCallback = onStaffInteractionCallback;
    }

    public StaffEntryListAdapter(RealmResults<SchoolStaff> paramStaffs){
        staffRealmResults = paramStaffs;
    }

    @Override
    public StaffEntryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.staff_list_item, parent, false);
        return new StaffEntryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StaffEntryListAdapter.MyViewHolder holder, int position) {
        final SchoolStaff staff = staffRealmResults.get(position);
        holder.staffName.setText(staff.getName());
        holder.staffNumber.setText(Utils.maskNumber(staff.getPhone(), "xxxxxxx###"));
        holder.staffType.setText(staff.getDesignation());
        holder.relativeLayout.setTag(staff);
        holder.staffExit.setTag(staff);
        if(staff.isExited()){
            holder.staffExit.setVisibility(View.GONE);
        }else{
            holder.staffExit.setVisibility(View.VISIBLE);
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolStaff staffClicked = (SchoolStaff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffClicked(staffClicked);
                }
            }
        });
        holder.staffExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolStaff staffClicked = (SchoolStaff) view.getTag();
                if(onStaffInteractionCallback != null){
                    onStaffInteractionCallback.onStaffExited(staffClicked);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return staffRealmResults.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        public TextView staffName;
        public TextView staffNumber;
        public TextView staffType;
        public TextView staffExit;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            staffName = (TextView) view.findViewById(R.id.text_staff_name);
            staffNumber = (TextView) view.findViewById(R.id.text_staff_number);
            staffType = (TextView) view.findViewById(R.id.text_type);
            staffExit = (TextView) view.findViewById(R.id.text_exit_staff);
        }
    }

}

