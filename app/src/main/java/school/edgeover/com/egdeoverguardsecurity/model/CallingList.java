package school.edgeover.com.egdeoverguardsecurity.model;

/**
 * Created by anshul on 24/04/18.
 */

public class CallingList {

    private String name;
    private String number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
