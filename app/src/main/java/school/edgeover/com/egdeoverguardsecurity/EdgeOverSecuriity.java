package school.edgeover.com.egdeoverguardsecurity;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;

/**
 * Created by anshul on 13/03/18.
 */

public class EdgeOverSecuriity extends Application {

    //private static GoogleAnalytics sAnalytics;
   // private static Tracker sTracker;
    private static EdgeOverSecuriity sCurrentInstance = null;

    @Override
    public void onCreate() {
        super.onCreate();
       // Fabric.with(this, new Crashlytics());
        sCurrentInstance = this;
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .migration(new RealmMigration() {
                    @Override
                    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
                        if(oldVersion == 0){
                            RealmObjectSchema schoolStaffScema = Realm.getDefaultInstance().getSchema().get("SchoolStaff");
                            // Combine 'firstName' and 'lastName' in a new field called 'fullName'
                            schoolStaffScema.addField("aid", String.class, FieldAttribute.REQUIRED)
                                    .transform(new RealmObjectSchema.Function() {
                                        @Override
                                        public void apply(DynamicRealmObject obj) {
                                        }
                                    });
                            oldVersion++;
                        }
                    }
                }).schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
        //sAnalytics = GoogleAnalytics.getInstance(this);
       // getDefaultTracker();


    }

    @Override
    public void onTerminate() {
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    public static synchronized EdgeOverSecuriity getCurrentInstance() {
        return sCurrentInstance;
    }



    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     *//*
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }*/
}

