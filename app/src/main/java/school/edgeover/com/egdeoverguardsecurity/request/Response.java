package school.edgeover.com.egdeoverguardsecurity.request;

/**
 * Created by utsavjain on 24/02/18.
 */

public class Response {
    boolean status;
    String data;
    String message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
