package school.edgeover.com.egdeoverguardsecurity.parser;

import org.json.JSONException;
import org.json.JSONObject;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.model.Alert;

/**
 * Created by anshul on 27/12/17.
 */

public class AlertParser {

    public static Alert parseAlertData(String response){
        Alert alert = new Alert();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null){
                JSONObject residentJsonObject = jsonObject.optJSONObject(Keys.RESIDENT);
                if(residentJsonObject != null){
                    alert.setFlatName(residentJsonObject.optString(Keys.ADDRESS1) + " - " + residentJsonObject.optString(Keys.ADDRESS2));
                    alert.setNumber(residentJsonObject.optString(Keys.PHONE1));
                    alert.setIntercomNumber(residentJsonObject.optString(Keys.PHONE2));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return alert;
    }
}
