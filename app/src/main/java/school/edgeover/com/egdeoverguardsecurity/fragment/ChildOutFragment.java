package school.edgeover.com.egdeoverguardsecurity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.Realm;
import io.realm.RealmResults;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.adapter.StaffEntryListAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnStaffInteractionCallback;
import school.edgeover.com.egdeoverguardsecurity.dialog.ChildInfoDialog;
import school.edgeover.com.egdeoverguardsecurity.dialog.StaffInfoDialog;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 24/03/18.
 */

public class ChildOutFragment extends Fragment implements OnStaffInteractionCallback {
    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<SchoolStaff> staffs;
    private StaffEntryListAdapter staffEntryListAdapter;
    public static boolean isCacheDirty;


    public ChildOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_out, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        isCacheDirty = false;
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",true)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        staffEntryListAdapter = new StaffEntryListAdapter(staffs);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(staffEntryListAdapter);
        staffEntryListAdapter.setOnStaffInteractionCallback(this);
    }

    public void makeCacheDirty(boolean val){
        isCacheDirty = val;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser && isCacheDirty){
            refreshStaffData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void refreshStaffData(){
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",true)
                .findAll();
        if(staffEntryListAdapter != null){
            staffEntryListAdapter.setStaffRealmResults(staffs);
            staffEntryListAdapter.notifyDataSetChanged();
            makeCacheDirty(false);
        }

    }

    public void loadDefaultData(){
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",true)
                .findAll();
        if(staffs != null && staffs.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(staffs);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        ChildOutFragment visitorOutFragment = new ChildOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    public void refreshVisitorsDataOnQuery(String name){
        RealmResults<SchoolStaff> queriedVisitors = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .contains("name",name)
                .equalTo("isChild",true)
                .equalTo("isExited",true)
                .findAll();
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(queriedVisitors);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        ChildOutFragment staffOutFragment = new ChildOutFragment();
        staffOutFragment.makeCacheDirty(true);
    }

    @Override
    public void onStaffExited(SchoolStaff staff) {

    }

    @Override
    public void onStaffClicked(SchoolStaff staff) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ChildInfoDialog staffInfoDialog = new ChildInfoDialog();
        staffInfoDialog.setStaffData(staff);
        staffInfoDialog.setCancelable(true);
        staffInfoDialog.show(fm, "Sample Fragment");
    }
}


