/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import school.edgeover.com.egdeoverguardsecurity.Utils.NavigationUtils;
import school.edgeover.com.egdeoverguardsecurity.Utils.PreferenceManager;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.adapter.RecyclerViewFacilitiesAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnFacilityEventClicked;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.AllSchoolData;
import school.edgeover.com.egdeoverguardsecurity.model.AllSocietyData;
import school.edgeover.com.egdeoverguardsecurity.model.BaseModel;
import school.edgeover.com.egdeoverguardsecurity.model.Facilities;
import school.edgeover.com.egdeoverguardsecurity.parser.AllSchoolDataParser;
import school.edgeover.com.egdeoverguardsecurity.parser.BaseModelParser;
import school.edgeover.com.egdeoverguardsecurity.widget.AutoFitRecyclerView;
import school.edgeover.com.egdeoverguardsecurity.widget.SpanSizeLokkup;

public class SocietyFacilitiesActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener ,OnFacilityEventClicked{

    private AutoFitRecyclerView mRecyclerView;
    private RecyclerViewFacilitiesAdapter mRecyclerViewFacilitiesAdapter;
    private ArrayList<Facilities> facilitiesArrayList;
    private TextView mTextHeaderTitle;
    private TextView mTextHeaderTitleSub;
    private TextView mTextHeaderEmail;
    private ImageView imageView;
    private AllSchoolData allSchoolData;
    AllSocietyData allSocietyData;
    private SensorManager sensorManager;
    private long lastUpdate;
    private FloatingActionButton floatingActionButton;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_society_facilities);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (AutoFitRecyclerView) findViewById(R.id.recycler_facilities);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        String mode = PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE);
        if (mode.equalsIgnoreCase(Constants.MODE_GUARD)) {
            getSupportActionBar().setTitle("Welcome Gate Guard");
        } else if (mode.equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            getSupportActionBar().setTitle("Welcome " + PreferenceManager.getInstance(this).getString(PreferenceManager.USER_NAME));
        } else if (mode.equalsIgnoreCase(Constants.MODE_RWA)) {
            getSupportActionBar().setTitle(PreferenceManager.getInstance(this).getString(PreferenceManager.USER_NAME) + " RWA");
        }




        floatingActionButton = findViewById(R.id.fab_panic);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //updateUIOnActivity();
            }
        });



        setData();
        setRecyclerViewData();
        setShakeListener();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }

    private void showAlertDialogNotified() {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Guard Alerted");
            alertDialogBuilder
                    .setMessage("Guard has been notified , help will reach you shortly")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
    }

    private void setShakeListener() {
       /* sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lastUpdate = System.currentTimeMillis();
        shakeSensor = new ShakeSensor(lastUpdate);
        shakeSensor.setSensorInteractionListener(this);*/
    }


    private void setData() {
        facilitiesArrayList = new ArrayList<>();

        if (PreferenceManager.getInstance(SocietyFacilitiesActivity.this).
                getString(PreferenceManager.IS_LOGGED_IN_MODE).equalsIgnoreCase(Constants.MODE_GUARD)) {
            mRecyclerView.setVisibility(View.VISIBLE);
            Facilities facilities6 = new Facilities();
            facilities6.setIconID(R.drawable.create_visitor);
            facilities6.setFacilityName(Constants.VISITOR_ENTRY.toUpperCase());
            facilitiesArrayList.add(facilities6);

            Facilities facilities9 = new Facilities();
            facilities9.setIconID(R.drawable.a_register);
            facilities9.setFacilityName(Constants.VISITOR_REGISTER.toUpperCase());
            facilitiesArrayList.add(facilities9);

            Facilities facilities14 = new Facilities();
            facilities14.setIconID(R.drawable.a);
            facilities14.setFacilityName(Constants.STAFF_ATTENDENCE.toUpperCase());
            facilitiesArrayList.add(facilities14);

            Facilities facilities10 = new Facilities();
            facilities10.setIconID(R.drawable.a_register);
            facilities10.setFacilityName(Constants.STAFF_REGISTER.toUpperCase());
            facilitiesArrayList.add(facilities10);

            Facilities facilities8 = new Facilities();
            facilities8.setIconID(R.drawable.daycare_attendance);
            facilities8.setFacilityName(Constants.ATTENDENCE.toUpperCase());
            facilitiesArrayList.add(facilities8);

            Facilities facilities12 = new Facilities();
            facilities12.setIconID(R.drawable.a_register);
            facilities12.setFacilityName(Constants.CHILD_REGISTER.toUpperCase());
            facilitiesArrayList.add(facilities12);
        }
    }

    private void setRecyclerViewData() {
        mRecyclerViewFacilitiesAdapter = new RecyclerViewFacilitiesAdapter(this,facilitiesArrayList);
        mRecyclerViewFacilitiesAdapter.setOnFacilityEventClicked(this);
        if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
            int spanCount = 2;

            mRecyclerView.setExternalSpanCount(spanCount);
            ((GridLayoutManager) mRecyclerView.getLayoutManager())
                    .setSpanCount(2);
            ((GridLayoutManager) mRecyclerView.getLayoutManager()).setSpanSizeLookup(new SpanSizeLokkup());
        }
        mRecyclerView.setAdapter(mRecyclerViewFacilitiesAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_logout) {
            NavigationUtils.navigateToLogout(this);
            NavigationUtils.navigateToModeLogin(this);
            finish();
            Utils.displayToast(getApplicationContext(), "You have been logged out successfully");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFacilityClicked(String facilityName) {
        if (facilityName.equalsIgnoreCase(Constants.VISITOR_ENTRY)) {
            NavigationUtils.navigateToVisitorEntry(this, allSchoolData);
        } else if (facilityName.equalsIgnoreCase(Constants.VISITOR_REGISTER)) {
            NavigationUtils.navigateToVisitorEntryRegister(this);
        } else if (facilityName.equalsIgnoreCase(Constants.ATTENDENCE)) {
            NavigationUtils.navigateToAttendence(this, allSchoolData);
        } else if (facilityName.equalsIgnoreCase(Constants.STAFF_ATTENDENCE)) {
            NavigationUtils.navigateToStaffAttendence(this, allSchoolData);
        }  else if (facilityName.equalsIgnoreCase(Constants.STAFF_REGISTER)) {
            NavigationUtils.navigateToStaffEntryRegister(this);
        }else if (facilityName.equalsIgnoreCase(Constants.CHILD_REGISTER)) {
            NavigationUtils.navigateToChildEntryRegister(this);
        }

    }

    private void storeDataInPrefs(String response){
       PreferenceManager.getInstance(this).writeToPrefs(PreferenceManager.ALL_SCHOOL_RESPONSE,response);
    }



    private void configureAppData(String url) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            showProgressDialog("Please wait while we are configuring the app", false);
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            removeProgressDialog();
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                storeDataInPrefs(response);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                allSchoolData = AllSchoolDataParser.getAllSchoolData(response);
                                loadData(allSchoolData);

                            }
                            Utils.displayToast(SocietyFacilitiesActivity.this, baseModel.getMessage());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            removeProgressDialog();
                            Utils.displayToast(getApplicationContext(), getString(R.string.error_server));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        } else {
            Utils.displayToast(getApplicationContext(), getString(R.string.internet_error));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSOSSms();
                }
                return;
            }
        }
    }

    private void checkForSendSmsPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.SEND_SMS)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                            new String[]{Manifest.permission
                                                    .SEND_SMS},
                                            Constants.REQUEST_PERMISSIONS);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            new String[]{Manifest.permission
                                    .SEND_SMS},
                            Constants.REQUEST_PERMISSIONS);
                }
            }
        } else {
            //Call whatever you want
            sendSOSSms();
        }
    }

    private void triggerAlert() {
        if (Utils.isInternetAvailable(getApplicationContext())) {

            String url = Constants.BASE_URL + (Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_RESIDENT) ? Constants.RESIDENT_ALERT : Constants.ENTRY_ALERT);
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                showAlertDialogNotified();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Utils.displayToast(getApplicationContext(), getString(R.string.error_server));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

            };
            queue.add(postRequest);
        }
    }

    private void sendSOSSms() {
        String emergencyNumbers = PreferenceManager.getInstance(this).getString(PreferenceManager.EMERGENCY_NUMBERS
                , "");
        if (!TextUtils.isEmpty(emergencyNumbers)) {
            if (emergencyNumbers.contains(",")) {
                String[] numSet = emergencyNumbers.split(",");
                if (numSet != null && numSet.length > 0) {
                    for (String num : numSet) {
                        if (!TextUtils.isEmpty(num)) {
                            sendSms(num);
                        }
                    }
                }
            } else {
                sendSms(emergencyNumbers);
            }
        } else {
            Utils.displayToast(SocietyFacilitiesActivity.this, "Please add SOS contacts");
        }
    }

    private void sendSms(String number) {
       /* String messageToSend = "I need some help. It's an emergency";
        SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null, null);*/
    }

    @Override
    protected void onStop() {
        if (PreferenceManager.getInstance(this).getString(PreferenceManager.IS_LOGGED_IN_MODE, "")
                .equalsIgnoreCase(Constants.MODE_RESIDENT)) {
        }
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
       if(!TextUtils.isEmpty(PreferenceManager.getInstance(this).
                getString(PreferenceManager.ALL_SCHOOL_RESPONSE,""))){
            allSchoolData = AllSchoolDataParser.getAllSchoolData(PreferenceManager.getInstance(this).
                    getString(PreferenceManager.ALL_SCHOOL_RESPONSE,""));
                loadData(allSchoolData);

        } else {
               String url = Constants.BASE_URL + Constants.GET_ALL_GUARD_DATA;
               configureAppData(url);
        }


        super.onResume();
    }

    private void loadData(AllSchoolData allSchoolData){
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        mTextHeaderTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_title);
        mTextHeaderTitleSub = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_sub);
        mImageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        mTextHeaderTitleSub.setText("Welcome " + allSchoolData.getAdmissionStaff().getName());

        if (imageView != null && !TextUtils.isEmpty(PreferenceManager.getInstance(this).getString(PreferenceManager.LOGO))) {
            String url = Constants.MEDIA_BASE_URL + PreferenceManager.getInstance(this).getString(PreferenceManager.LOGO);
            Picasso.with(this)
                    .load(url)
                    .placeholder(R.drawable.app_icon)
                    .error(R.drawable.app_icon)
                    .into(imageView);
        }

        mTextHeaderEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_nav_name);
        mTextHeaderEmail.setText(allSchoolData.getAdmissionStaff().getEmail());
        navigationView.setNavigationItemSelectedListener(this);
    }
}
