package school.edgeover.com.egdeoverguardsecurity.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 201101101 on 10/17/2017.
 */

public class PreferenceManager {

    private static PreferenceManager preferenceManager;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor prefsEditor;

    public static final String DEVICE_USER_TOKEN = "device_token";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_FLAT = "user_flat";
    public static final String USER_NUMBER = "user_NUMBER";
    public static final String USER_REGISTER_NUMBER = "user_number";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String IS_LOGGED_IN_MODE = "is_logged_in_mode";
    public static final String IDENTIFICATION = "identification";
    public static final String EMERGENCY_NUMBERS = "emergency_numbers";
    public static final String ALL_SCHOOL_RESPONSE = "all_school_response";
    public static final String RESIDENT_DIRECTORY = "resident_directory";
    public static final String SOCIETY_ADDRESS_1 = "society_address_1";
    public static final String SOCIETY_ADDRESS_2 = "society_address_2";
    public static final String SOCIETY_LATTITUDE = "society_lattitude";
    public static final String SOCIETY_LONGITUDE = "society_longitude";

    public static final String ID = "id";
    public static final String TOKEN = "token";
    public static final String LOGO = "logo";

    private PreferenceManager(Context context) {
        preferences = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = preferences.edit();
    }

    public static PreferenceManager getInstance(Context context) {
        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager(context.getApplicationContext());
        }
        prefsEditor = preferences.edit();
        return preferenceManager;
    }

    public void writeToPrefs(String key, String val) {
        if (val == null) val = "";

        prefsEditor.putString(key, val);
        prefsEditor.apply();
    }

    public void writeToPrefs(String key, long val) {
        prefsEditor.putLong(key, val);
        prefsEditor.apply();
    }

    public void writeToPrefs(String key, int val) {
        prefsEditor.putInt(key, val);
        prefsEditor.apply();
    }

    public void writeToPrefs(String key, boolean val) {
        prefsEditor.putBoolean(key, val);
        prefsEditor.apply();
    }

    public void remove(String key) {
        prefsEditor.remove(key);
        prefsEditor.apply();
    }

    public String getString(String key) {
        return preferences.getString(key, null);
    }

    public boolean contains(String key) {
        return preferences.contains(key);
    }

    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    public int getInt(String key) {
        return preferences.getInt(key, -1);
    }

    public int getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }

    public boolean getBool(String key) {
        return preferences.getBoolean(key, false);
    }

    public boolean getBool(String key,boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    public long getLong(String key) {
        return preferences.getLong(key, -1L);
    }

    public long getLong(String key, long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }
}
