package school.edgeover.com.egdeoverguardsecurity.callbacks;

/**
 * Created by anshul on 24/04/18.
 */

public interface OnCallListClickedCallback {

    public void onCallMake(String number);
}
