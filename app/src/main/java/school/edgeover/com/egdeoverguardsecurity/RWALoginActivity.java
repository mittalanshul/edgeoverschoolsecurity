package school.edgeover.com.egdeoverguardsecurity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import school.edgeover.com.egdeoverguardsecurity.Utils.NavigationUtils;
import school.edgeover.com.egdeoverguardsecurity.Utils.PreferenceManager;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.BaseModel;
import school.edgeover.com.egdeoverguardsecurity.model.User;
import school.edgeover.com.egdeoverguardsecurity.parser.AuthorityLoginParser;
import school.edgeover.com.egdeoverguardsecurity.parser.BaseModelParser;

/**
 * A login screen that offers login via email/password.
 */
public class RWALoginActivity extends BaseActivity implements OnClickListener {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private String mode;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    // UI references.
    private AutoCompleteTextView mUserName;
    private AutoCompleteTextView mUserNameGuard;
    private EditText mPasswordView;
    private EditText mEditTextMobile;
    private View mProgressView;
    private View mLoginFormView;
    private TextView mTextSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Utils.isUserLoggedIn(this)) {
            NavigationUtils.navigateToSocietyFacilities(this);
        }
        setContentView(R.layout.activity_rwa_login);
        // Set up the login form.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("LOGIN");
        mUserName = (AutoCompleteTextView) findViewById(R.id.username);
        mUserNameGuard = (AutoCompleteTextView) findViewById(R.id.username_guard);
        mPasswordView = (EditText) findViewById(R.id.password);
        mEditTextMobile = (EditText) findViewById(R.id.mobile_number);
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        Button mSendOTP = (Button) findViewById(R.id.send_otp);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mTextSignUp = (TextView) findViewById(R.id.text_sign_in);
        mTextSignUp.setOnClickListener(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mode = getIntent().getStringExtra("mode");
        }

        if (mode.equalsIgnoreCase(Constants.MODE_GUARD)) {
            mUserName.setVisibility(View.GONE);
            mUserNameGuard.setVisibility(View.VISIBLE);
            mEditTextMobile.setVisibility(View.GONE);
            mSendOTP.setVisibility(View.GONE);
            mUserName.setHint("Guard Username");
            mUserName.setCompletionHint("Guard Username");
        } else if (mode.equalsIgnoreCase(Constants.MODE_RESIDENT)) {
            mUserName.setVisibility(View.GONE);
            mUserNameGuard.setVisibility(View.GONE);
            mPasswordView.setVisibility(View.GONE);
            mTextSignUp.setVisibility(View.GONE);
            mEmailSignInButton.setVisibility(View.GONE);
            mUserName.setHint("Resident Username");
        } else {
            mUserName.setVisibility(View.VISIBLE);
            mUserNameGuard.setVisibility(View.GONE);
            mEditTextMobile.setVisibility(View.GONE);
            mSendOTP.setVisibility(View.GONE);
        }


        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mSendOTP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isNumberValid(mEditTextMobile.getText().toString().trim(),getApplicationContext())) {
                    checkForAutoReadPermission();
                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        mUserName.setError(null);
        mUserNameGuard.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email =  mUserName.getText().toString();
        if(mode.equalsIgnoreCase(Constants.MODE_GUARD)){
            email =   mUserNameGuard.getText().toString();;
        }

        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;
        // Check for a valid email address.
        if (TextUtils.isEmpty(email) && mode.equalsIgnoreCase(Constants.MODE_RWA)) {
            mUserName.setError(getString(R.string.error_field_required));
            focusView = mUserName;
            cancel = true;
        }else  if (TextUtils.isEmpty(email) && mode.equalsIgnoreCase(Constants.MODE_GUARD)) {
            mUserNameGuard.setError(getString(R.string.error_field_required));
            focusView = mUserNameGuard;
            cancel = true;
        } else  if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }



        if (cancel) {
            focusView.requestFocus();
        } else {

            JSONObject jsonBody = new JSONObject();
            try {
                if(mode.equalsIgnoreCase(Constants.MODE_GUARD)){
                    jsonBody.put("id",mUserNameGuard.getText().toString().trim());
                }else if(mode.equalsIgnoreCase(Constants.MODE_RWA)){
                    jsonBody.put("id",mUserName.getText().toString().trim());
                }
                jsonBody.put("password",mPasswordView.getText().toString().trim());
                jsonBody.put("app_id",PreferenceManager.getInstance(getApplicationContext())
                        .getString(PreferenceManager.DEVICE_USER_TOKEN));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            if(Utils.isInternetAvailable(getApplicationContext())){
                showProgressDialog("Logging in",false);
                String url = "";
                if(mode.equalsIgnoreCase(Constants.MODE_RWA)){
                    url = Constants.BASE_URL +
                            Constants.AUTHORITY_lOGIN;
                }else if(mode.equalsIgnoreCase(Constants.MODE_GUARD)){
                    url = Constants.BASE_URL +
                            Constants.GUARD_lOGIN;
                }
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                removeProgressDialog();
                                BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                                if(baseModel.isStatus()){
                                    User user = AuthorityLoginParser.parserAuthorityLogin(response);
                                    if(user != null){
                                        Utils.saveUserData(getApplicationContext(),user,mode);
                                        NavigationUtils.navigateToSocietyFacilities(RWALoginActivity.this);
                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                }
                                Utils.displayToast(RWALoginActivity.this,baseModel.getMessage());


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                removeProgressDialog();
                                Utils.displayToast(getApplicationContext(),getString(R.string.error_server));
                            }
                        }
                ) {

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                            return null;
                        }
                    }

                };
                queue.add(postRequest);
            }else{
                Utils.displayToast(getApplicationContext(),"Please check your internet connection");
            }


        }
    }

    private void checkForAutoReadPermission() {

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_SMS)) {
                Snackbar.make(this.findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                            new String[]{Manifest.permission
                                                    .READ_SMS},
                                            Constants.REQUEST_PERMISSIONS);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            new String[]{Manifest.permission
                                    .READ_SMS},
                            Constants.REQUEST_PERMISSIONS);
                }
            }
        } else {
            //Call whatever you want
           // showOTPFragment(mEditTextMobile.getText().toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_PERMISSIONS: {
                  //  showOTPFragment(mEditTextMobile.getText().toString());
                return;
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_sign_in:
                //NavigationUtils.navigateToSignUp(this);
                break;
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }
}

