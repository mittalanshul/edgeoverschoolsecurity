package school.edgeover.com.egdeoverguardsecurity.model;

/**
 * Created by 201101101 on 11/24/2017.
 */

public class BaseModel {

    private String data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
