package school.edgeover.com.egdeoverguardsecurity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import school.edgeover.com.egdeoverguardsecurity.BaseActivity;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.adapter.StaffEntryListAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnStaffInteractionCallback;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.dialog.ChildInfoDialog;
import school.edgeover.com.egdeoverguardsecurity.dialog.StaffInfoDialog;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 24/03/18.
 */

public class ChildInFragment extends Fragment implements OnStaffInteractionCallback {

    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<SchoolStaff> staffRealmResults;
    private StaffEntryListAdapter staffEntryListAdapter;
    private RealmChangeListener realmListener;


    public ChildInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_in, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        staffRealmResults = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",false)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        staffEntryListAdapter = new StaffEntryListAdapter(staffRealmResults);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(staffEntryListAdapter);
        staffEntryListAdapter.setOnStaffInteractionCallback(this);
    }



    private void refreshStaffEntryData(){
        staffRealmResults = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",false)
                .findAll();
        if(staffRealmResults != null && staffRealmResults.size() >0){
            setmRecyclerViewData();
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        ChildOutFragment childOutFragment = new ChildOutFragment();
        childOutFragment.makeCacheDirty(true);
    }

    public void loadDefaultData(){
        staffRealmResults = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",true)
                .equalTo("isExited",false)
                .findAll();
        if(staffRealmResults != null && staffRealmResults.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(staffRealmResults);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        ChildOutFragment visitorOutFragment = new ChildOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    public void refreshStaffDataOnQuery(String name){
        RealmResults<SchoolStaff> queriedVisitors = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .contains("name",name)
                .equalTo("isChild",true)
                .equalTo("isExited",false)
                .findAllSorted("date", Sort.ASCENDING);
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(queriedVisitors);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        ChildOutFragment staffOutFragment = new ChildOutFragment();
        staffOutFragment.makeCacheDirty(true);
    }

    @Override
    public void onStaffExited(SchoolStaff staff) {
        if(staff != null){
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            SchoolStaff staffToEdit = realm.where(SchoolStaff.class)
                    .equalTo("id", staff.getId())
                    .equalTo("isChild", true)
                    .equalTo("isExited",false)
                    .findFirst();
            staffToEdit.setExited(true);
            staffToEdit.setDateExited(Utils.getCurrentDate());
            staffToEdit.setTimeExited(Utils.getCurrentTime());
            realm.commitTransaction();
            refreshStaffEntryData();
            makeStaffEntryOnServer(staff);

        }


    }

    private void makeStaffEntryOnServer(SchoolStaff staff){
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id",staff.getId());
            jsonBody.put("out", Utils.getEntryTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = jsonBody.toString();
        if(Utils.isInternetAvailable(getActivity())){
            String url = Constants.BASE_URL + Constants.ENTRY_OUT ;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getActivity().getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }
            };
            ((BaseActivity)getActivity()).queue.add(postRequest);
        }
    }

    @Override
    public void onStaffClicked(SchoolStaff staff) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ChildInfoDialog staffInfoDialog = new ChildInfoDialog();
        staffInfoDialog.setStaffData(staff);
        staffInfoDialog.setCancelable(true);
        staffInfoDialog.show(fm, "Sample Fragment");
    }
}


