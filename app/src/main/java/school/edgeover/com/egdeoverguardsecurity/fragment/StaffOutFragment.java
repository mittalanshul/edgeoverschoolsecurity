/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.logging.Logger;

import io.realm.Realm;
import io.realm.RealmResults;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.adapter.StaffEntryListAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnStaffInteractionCallback;
import school.edgeover.com.egdeoverguardsecurity.dialog.StaffInfoDialog;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.Staff;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffOutFragment extends Fragment implements OnStaffInteractionCallback {
    private RecyclerView mRecyclerView;
    private View mView;
    private RealmResults<SchoolStaff> staffs;
    private StaffEntryListAdapter staffEntryListAdapter;
    public static boolean isCacheDirty;


    public StaffOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_out, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViews();
        isCacheDirty = false;
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",false)
                .equalTo("isExited",true)
                .findAll();
        setmRecyclerViewData();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews(){
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.visitor_recyclerview);
    }

    private void setmRecyclerViewData(){
        staffEntryListAdapter = new StaffEntryListAdapter(staffs);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(staffEntryListAdapter);
        staffEntryListAdapter.setOnStaffInteractionCallback(this);

    }

    public void makeCacheDirty(boolean val){
        isCacheDirty = val;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser && isCacheDirty){
            refreshStaffData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void refreshStaffData(){
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",false)
                .equalTo("isExited",true)
                .findAll();
        if(staffEntryListAdapter != null){
            staffEntryListAdapter.setStaffRealmResults(staffs);
            staffEntryListAdapter.notifyDataSetChanged();
            makeCacheDirty(false);
        }

    }

    public void loadDefaultData(){
        staffs = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .equalTo("isChild",false)
                .equalTo("isExited",true)
                .findAll();
        if(staffs != null && staffs.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(staffs);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        StaffOutFragment visitorOutFragment = new StaffOutFragment();
        visitorOutFragment.makeCacheDirty(true);
    }

    public void refreshVisitorsDataOnQuery(String name){
        RealmResults<SchoolStaff> queriedVisitors = Realm.getDefaultInstance()
                .where(SchoolStaff.class)
                .contains("name",name)
                .equalTo("isChild",false)
                .equalTo("isExited",true)
                .findAll();
        if(queriedVisitors != null && queriedVisitors.size() >0){
            if(staffEntryListAdapter != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                staffEntryListAdapter.setStaffRealmResults(queriedVisitors);
                staffEntryListAdapter.notifyDataSetChanged();
            }
        } else{
            mRecyclerView.setVisibility(View.GONE);
        }
        StaffOutFragment staffOutFragment = new StaffOutFragment();
        staffOutFragment.makeCacheDirty(true);
    }

    @Override
    public void onStaffExited(SchoolStaff staff) {
        Logger.getLogger("anshul : on staff exited called");

    }

    @Override
    public void onStaffClicked(SchoolStaff staff) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        StaffInfoDialog staffInfoDialog = new StaffInfoDialog();
        staffInfoDialog.setStaffData(staff);
        staffInfoDialog.setCancelable(true);
        staffInfoDialog.show(fm, "Sample Fragment");
    }
}

