package school.edgeover.com.egdeoverguardsecurity.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by anshul on 24/03/18.
 */

public class AllSchoolData implements Parcelable{

    private ArrayList<SchoolStaff> teacherArrayList;
    private ArrayList<SchoolStaff> childArrayLst;
    private ArrayList<SchoolStaff> staffArrayList;

    public ArrayList<SchoolStaff> getDomesticStaffArrayList() {
        return domesticStaffArrayList;
    }

    public void setDomesticStaffArrayList(ArrayList<SchoolStaff> domesticStaffArrayList) {
        this.domesticStaffArrayList = domesticStaffArrayList;
    }

    private ArrayList<SchoolStaff> domesticStaffArrayList;

    protected AllSchoolData(Parcel in) {
        teacherArrayList = in.createTypedArrayList(SchoolStaff.CREATOR);
        childArrayLst = in.createTypedArrayList(SchoolStaff.CREATOR);
        staffArrayList = in.createTypedArrayList(SchoolStaff.CREATOR);
        domesticStaffArrayList = in.createTypedArrayList(SchoolStaff.CREATOR);
        admissionStaff = in.readParcelable(SchoolStaff.class.getClassLoader());
        name = in.readString();
        email = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        phone1 = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(teacherArrayList);
        dest.writeTypedList(childArrayLst);
        dest.writeTypedList(staffArrayList);
        dest.writeTypedList(domesticStaffArrayList);
        dest.writeParcelable(admissionStaff, flags);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(phone1);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AllSchoolData> CREATOR = new Creator<AllSchoolData>() {
        @Override
        public AllSchoolData createFromParcel(Parcel in) {
            return new AllSchoolData(in);
        }

        @Override
        public AllSchoolData[] newArray(int size) {
            return new AllSchoolData[size];
        }
    };

    public SchoolStaff getAdmissionStaff() {
        return admissionStaff;
    }

    public void setAdmissionStaff(SchoolStaff admissionStaff) {
        this.admissionStaff = admissionStaff;
    }

    private SchoolStaff admissionStaff;

    private String name;
    private String email;
    private String address1;
    private String address2;
    private String phone1;
    private String latitude;
    private String longitude;
    private String id;

    public ArrayList<SchoolStaff> getTeacherArrayList() {
        return teacherArrayList;
    }

    public void setTeacherArrayList(ArrayList<SchoolStaff> teacherArrayList) {
        this.teacherArrayList = teacherArrayList;
    }

    public ArrayList<SchoolStaff> getChildArrayLst() {
        return childArrayLst;
    }

    public void setChildArrayLst(ArrayList<SchoolStaff> childArrayLst) {
        this.childArrayLst = childArrayLst;
    }

    public ArrayList<SchoolStaff> getStaffArrayList() {
        return staffArrayList;
    }

    public void setStaffArrayList(ArrayList<SchoolStaff> staffArrayList) {
        this.staffArrayList = staffArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AllSchoolData(){

    }

}
