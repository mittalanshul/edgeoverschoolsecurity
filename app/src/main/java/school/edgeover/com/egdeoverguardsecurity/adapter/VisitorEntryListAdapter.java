/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.RealmResults;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnVisitorInteractionCallbacks;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;
import school.edgeover.com.egdeoverguardsecurity.widget.CircularImageView;


/**
 * Created by 201101101 on 10/30/2017.
 */

public class VisitorEntryListAdapter extends RecyclerView.Adapter<VisitorEntryListAdapter.MyViewHolder> {
    public RealmResults<Visitor> getVisitorsList() {
        return visitorsList;
    }

    public void setVisitorsList(RealmResults<Visitor> visitorsList) {
        this.visitorsList = visitorsList;
    }

    private RealmResults<Visitor> visitorsList;
    private OnVisitorInteractionCallbacks onVisitorInteractionCallbacks;
    private Context mContext;


    public OnVisitorInteractionCallbacks getOnVisitorInteractionCallbacks() {
        return onVisitorInteractionCallbacks;
    }

    public void setOnVisitorInteractionCallbacks(OnVisitorInteractionCallbacks onVisitorInteractionCallbacks) {
        this.onVisitorInteractionCallbacks = onVisitorInteractionCallbacks;
    }



    public VisitorEntryListAdapter(RealmResults<Visitor> paramVisitors , Context paramContext){
        visitorsList = paramVisitors;
        mContext = paramContext;
    }

    @Override
    public VisitorEntryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.visitor_list_item, parent, false);
        return new VisitorEntryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Visitor visitor = visitorsList.get(position);
        holder.visitorName.setText(visitor.getVisitorName());
        holder.visitorNumber.setText(visitor.getVisitorNumber());
        holder.visitorPurpose.setText(visitor.getPurpose());
        holder.relativeLayout.setTag(visitor);
        holder.visitorExit.setTag(visitor);
        holder.visitorExit.setText("EXIT");
        if(visitor.isExited()){
            holder.visitorExit.setVisibility(View.GONE);
        }else{
            holder.visitorExit.setVisibility(View.VISIBLE);
        }
        holder.visitorPurpose.setVisibility(View.GONE);
        holder.visitorPurposeLabel.setVisibility(View.GONE);
        holder.mImgDelete.setVisibility(View.GONE);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Visitor visitor = (Visitor) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onVisitorClicked(visitor);
                }
            }
        });
        holder.visitorExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Visitor visitor = (Visitor) view.getTag();
                if(onVisitorInteractionCallbacks != null){
                    onVisitorInteractionCallbacks.onVisitorExited(visitor);
                }
            }
        });

        if(holder.circularImageView != null && !TextUtils.isEmpty(visitor.getVisitorLocalImage())){
            Bitmap bmp = Utils.decodeBase64(visitor.getVisitorLocalImage());
            holder.circularImageView.setImageBitmap(bmp);
        }
    }

    @Override
    public int getItemCount() {
        return visitorsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout relativeLayout;
        public TextView visitorName;
        public TextView visitorNumber;
        public TextView visitorPurpose;
        public TextView visitorExit;
        public TextView visitorPurposeLabel;
        public CircularImageView circularImageView;
        public ImageView mImgDelete;

        public MyViewHolder(View view) {
            super(view);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.rel_visitor);
            visitorName = (TextView) view.findViewById(R.id.text_visitor_name);
            visitorNumber = (TextView) view.findViewById(R.id.text_visitor_number);
            visitorPurpose = (TextView) view.findViewById(R.id.text_purpose);
            visitorExit = (TextView) view.findViewById(R.id.text_exit_visitor);
            visitorPurposeLabel = (TextView) view.findViewById(R.id.text_purpose_label);
            circularImageView = (CircularImageView) view.findViewById(R.id.visitor_pic);
            mImgDelete = (ImageView)view.findViewById(R.id.img_deleter);
        }
    }

}
