package school.edgeover.com.egdeoverguardsecurity.parser;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;

/**
 * Created by anshul on 22/12/17.
 */

public class VisitorDetailParser {

    public static ArrayList<Visitor> getVisitorDetails(String response){
        ArrayList<Visitor> visitorArrayList = new ArrayList<>();
        try{
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject != null) {
                JSONArray dataJsonArray = jsonObject.optJSONArray(Keys.DATA);
                if(dataJsonArray != null && dataJsonArray.length()>0) {
                   for(int i =0 ; i<dataJsonArray.length();i++){
                       JSONObject jsonObject1 = dataJsonArray.optJSONObject(i);
                       if(jsonObject1 != null){
                           Visitor visitor = new Visitor();
                           visitor.setId(jsonObject1.optString(Keys.ID));
                           visitor.setVisitorName(jsonObject1.optString(Keys.NAME));
                           visitor.setVisitorNumber(jsonObject1.optString(Keys.PHONE));
                           visitor.setVehicleName(jsonObject1.optString(Keys.VEHICLE));
                           visitor.setPurpose(jsonObject1.optString(Keys.REASON));
                           visitor.setDate(jsonObject1.optString(Keys.ENTRY));
                           visitor.setDateExited(jsonObject1.optString(Keys.EXIT));
                           visitor.setFlatVisited(jsonObject1.optString(Keys.ADDRESS));
                           visitor.setVisitorPicUrl(jsonObject1.optString(Keys.IMAGE));
                           visitorArrayList.add(visitor);
                       }
                   }
                }
            }
        }catch (JSONException e){

        }
        return visitorArrayList;



    }
}
