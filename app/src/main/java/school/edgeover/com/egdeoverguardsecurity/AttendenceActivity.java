/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license,
 * You should have received a copy of the EdgeOver(A Unit of MFW Creations Pvt Ltd) license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import io.realm.Realm;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.adapter.VisitorEntryPagerAdapter;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnAttendenceListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.fragment.AttendenceListFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.VisitorInFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.VisitorOutFragment;
import school.edgeover.com.egdeoverguardsecurity.model.AllSchoolData;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.widget.CircularImageView;

public class AttendenceActivity extends BaseActivity implements View.OnClickListener ,OnAttendenceListClickedCallback {

    private String type;

    private MaterialBetterSpinner materialDesignSpinnerType;
    String[] TYPE_LIST = {Constants.CHILD};

    private Button mButtonCreateAttendence;
    private AllSchoolData allSchoolData;

    private EditText etIdNumber;
    private SchoolStaff selectedStaff;
    private SearchView mSearchView;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        if (savedInstanceState != null) {
            allSchoolData = savedInstanceState.getParcelable("all_data");
        } else {
            if (getIntent() != null && getIntent().getExtras() != null) {
                allSchoolData = getIntent().getParcelableExtra("all_data");
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.ATTENDENCE);

        materialDesignSpinnerType = findViewById(R.id.spinner_type);
        etIdNumber = findViewById(R.id.et_id_number);
        mButtonCreateAttendence = (Button) findViewById(R.id.create_attendence);
        mButtonCreateAttendence.setOnClickListener(this);

        etIdNumber.setHint("Staff Id Number");
        etIdNumber.setVisibility(View.GONE);

        materialDesignSpinnerType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type = adapterView.getItemAtPosition(i).toString();
                ArrayList<SchoolStaff> sourceList = null;
                if(type.equalsIgnoreCase(Constants.CHILD) && allSchoolData != null
                        && allSchoolData.getChildArrayLst()!= null && allSchoolData.getChildArrayLst().size() >0){
                    sourceList = allSchoolData.getChildArrayLst();
                }
                if (sourceList != null) {
                    showAttendenceFragment(sourceList);
                }

            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, TYPE_LIST);
        materialDesignSpinnerType.setAdapter(arrayAdapter);
    }

    private void showAttendenceFragment(ArrayList<SchoolStaff> schoolStaffArrayList) {
        if(Utils.isInternetAvailable(this)){
            AttendenceListFragment attendenceListFragment = new AttendenceListFragment();
            attendenceListFragment.setRetainInstance(true);
            attendenceListFragment.setSchoolStaffArrayList(schoolStaffArrayList);
            attendenceListFragment.setOnAttendenceListClickedCallback(this);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                    R.anim.exit_out_bottom).add(R.id.frame_login, attendenceListFragment, "otp")
                    .addToBackStack("otp").commitAllowingStateLoss();
        } else {
            Utils.displayToast(this,getString(R.string.internet_error));
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch= ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    AttendenceListFragment attendenceListFragment = (AttendenceListFragment)getSupportFragmentManager().findFragmentByTag("otp");
                    if(attendenceListFragment != null){
                        if(!TextUtils.isEmpty(query)){
                            attendenceListFragment.refreshDataOnSearch(query);
                        }else{
                            attendenceListFragment.loadDefaultData();
                        }
                    }

                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_attendence:
                if(selectedStaff != null){
                    markAttendence(selectedStaff.getId());
                    //makeAttendenceEntryOnServer(selectedStaff.getId());
                }

                break;
        }
    }

    private void makeAttendenceEntryOnServer(String id) {
        if (Utils.isInternetAvailable(this)) {
            String url = Constants.BASE_URL + Constants.CREATE_STAFF_ENTRY_URL;
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("in",Utils.getEntryTime());
                jsonBody.put("id", selectedStaff.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String mRequestBody = jsonBody.toString();
            //showProgressDialog("Marking Attendance", false);
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(getApplicationContext());
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }


            };
            queue.add(postRequest);
        }
    }

    private void showAttendenceCard(){
        findViewById(R.id.rel_domestic).setVisibility(View.VISIBLE);
        TextView label = (TextView)findViewById(R.id.label_domestic);
        TextView name = (TextView) findViewById(R.id.text_staff_name);
        TextView number = (TextView) findViewById(R.id.text_staff_number);
        CircularImageView circularImageView = (CircularImageView) findViewById(R.id.domestic_pic);

        if(!TextUtils.isEmpty(selectedStaff.getDesignation())){
            label.setVisibility(View.VISIBLE);
            label.setText(selectedStaff.getDesignation());
        }else{
            label.setVisibility(View.INVISIBLE);
        }

        name.setText(selectedStaff.getName());
        number.setText(Utils.maskNumber(selectedStaff.getPhone(), "xxxxxxx###"));
        String url = Constants.MEDIA_BASE_URL + selectedStaff.getImage();
        circularImageView.setImageDrawable(null);
        Picasso.with(this).load(url).placeholder(R.drawable.user_pic_def).error(R.drawable.user_pic_def)
                .into(circularImageView);
    }


    private void markAttendence(String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        SchoolStaff staffExists = realm.where(SchoolStaff.class)
                .equalTo("date", Utils.getCurrentDate())
                .contains("id",id)
                .findFirst();
        if(staffExists == null && selectedStaff != null) {
            SchoolStaff staff = realm.createObject(SchoolStaff.class);
            staff.setName(selectedStaff.getName());
            staff.setId(selectedStaff.getId());
            staff.setPhone(selectedStaff.getPhone());
            staff.setFatherName(selectedStaff.getFatherName());
            staff.setMotherName(selectedStaff.getMotherName());
            staff.setAddress(selectedStaff.getAddress());
            staff.setChild(true);
            staff.setExited(false);
            staff.setDate(Utils.getCurrentDate());
            staff.setTime(Utils.getCurrentTime());
            makeAttendenceEntryOnServer(id);
            Utils.displayToast(this, "ATTENDANCE MARKED");
        }else{
            Utils.displayToast(getApplicationContext(),"No School Staff found");
        }
        realm.commitTransaction();
        finish();

    }



    @Override
    public void onAttendenceClicked(SchoolStaff schoolStaff) {
        selectedStaff = schoolStaff;
        if(selectedStaff != null){
            dismissFragment();
            showAttendenceCard();
        }else{
            findViewById(R.id.rel_domestic).setVisibility(View.GONE);
        }
    }

    private void dismissFragment(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
}

