/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import io.realm.Realm;
import school.edgeover.com.egdeoverguardsecurity.Utils.Keys;
import school.edgeover.com.egdeoverguardsecurity.Utils.NavigationUtils;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnAttendenceListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.callbacks.OnCallListClickedCallback;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.fragment.AttendenceListFragment;
import school.edgeover.com.egdeoverguardsecurity.fragment.CallListFragment;
import school.edgeover.com.egdeoverguardsecurity.model.AllSchoolData;
import school.edgeover.com.egdeoverguardsecurity.model.BaseModel;
import school.edgeover.com.egdeoverguardsecurity.model.Contact;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;
import school.edgeover.com.egdeoverguardsecurity.model.Visitor;
import school.edgeover.com.egdeoverguardsecurity.request.VolleyMultipartRequest;
import school.edgeover.com.egdeoverguardsecurity.widget.CircularImageView;

public class VisitorEntryActivity extends BaseActivity implements View.OnClickListener,
        OnAttendenceListClickedCallback ,OnCallListClickedCallback{

    String[] TYPE_LIST = {Constants.TEACHER, Constants.STAFF};

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALLERY_CAPTURE = 3;
    static final int REQUEST_CALL = 4;
    static final int REQUEST_CODE_ACTION = 5;

    private Button mButtonCreateVistor;
    private TextView mTextVistorName;
    private TextView mTextVistorNumber;
    private AllSchoolData allSchoolData;
    private MaterialBetterSpinner materialDesignSpinner1;
    private MaterialBetterSpinner materialDesignSpinner;
    private MaterialBetterSpinner materialDesignSpinnerPurpose;
    private TextInputLayout mTextInputLayout;
    private String purpose;
    private CircularImageView circularImageView;
    private Bitmap visitorImageBitmap;

    private String typeSelected;
    private SchoolStaff selectedStaff;
    private SearchView mSearchView;
    private EditText mEditTextVendor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_entry);

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("E MMM dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        date.setTimeZone(istTimeZone);
        String localTime = date.format(currentLocalTime);

        if (savedInstanceState != null) {
            allSchoolData = savedInstanceState.getParcelable("all_data");
        } else {
            if (getIntent() != null && getIntent().getExtras() != null) {
                allSchoolData = getIntent().getParcelableExtra("all_data");
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Constants.VISITOR_ENTRY);

        initViews();

        takePhotoFromCamera();
        materialDesignSpinner.setVisibility(View.GONE);
        materialDesignSpinner1.setVisibility(View.GONE);


        if (allSchoolData != null) {
            materialDesignSpinner1.setVisibility(View.GONE);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, TYPE_LIST);
            materialDesignSpinner.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();

            materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    typeSelected = adapterView.getItemAtPosition(pos).toString();
                    ArrayList<SchoolStaff> sourceList;
                    if (typeSelected.equalsIgnoreCase(Constants.STAFF)) {
                        sourceList = allSchoolData.getStaffArrayList();
                    }else {
                        sourceList = allSchoolData.getTeacherArrayList();
                    }
                    if (sourceList != null) {
                        showAttendenceFragment(sourceList);
                    }
                }
            });
        }


        ArrayAdapter<String> purposeAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.purpose));
        materialDesignSpinnerPurpose.setAdapter(purposeAdapter);
        purposeAdapter.notifyDataSetChanged();
        materialDesignSpinnerPurpose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                purpose = adapterView.getItemAtPosition(i).toString();
                if (purpose.equalsIgnoreCase(Constants.ADMISSION)
                        || purpose.equalsIgnoreCase(Constants.ENQUIRE)) {
                    materialDesignSpinner.setVisibility(View.GONE);
                    materialDesignSpinner1.setVisibility(View.GONE);
                    mTextInputLayout.setVisibility(View.GONE);
                    selectedStaff = allSchoolData.getAdmissionStaff();
                } else if(purpose.equalsIgnoreCase(Constants.VENDOR)){
                    materialDesignSpinner.setVisibility(View.GONE);
                    materialDesignSpinner1.setVisibility(View.GONE);
                    mTextInputLayout.setVisibility(View.VISIBLE);
                } else {
                    materialDesignSpinner.setVisibility(View.VISIBLE);
                    materialDesignSpinner1.setVisibility(View.GONE);
                    mTextInputLayout.setVisibility(View.GONE);
                }
            }
        });

    }

    private void showAttendenceFragment(ArrayList<SchoolStaff> schoolStaffArrayList) {
        if (Utils.isInternetAvailable(this)) {
            AttendenceListFragment attendenceListFragment = new AttendenceListFragment();
            attendenceListFragment.setRetainInstance(true);
            attendenceListFragment.setSchoolStaffArrayList(schoolStaffArrayList);
            attendenceListFragment.setOnAttendenceListClickedCallback(this);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                    R.anim.exit_out_bottom).add(R.id.frame_login, attendenceListFragment, "otp")
                    .addToBackStack("otp").commitAllowingStateLoss();
        } else {
            Utils.displayToast(this, getString(R.string.internet_error));
        }

    }

    private void showCallingListFragment() {
        if (Utils.isInternetAvailable(this)) {
            CallListFragment callingListFragment = new CallListFragment();
            callingListFragment.setRetainInstance(true);
            callingListFragment.setOnCallListClickedCallback(this);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                    R.anim.exit_out_bottom).add(R.id.frame_login, callingListFragment, "call")
                    .addToBackStack("call").commitAllowingStateLoss();
        } else {
            Utils.displayToast(this, getString(R.string.internet_error));
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            FragmentManager fm = getSupportFragmentManager();
            if(getSupportFragmentManager().getBackStackEntryCount() > 0){
                for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
            }else{
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.spinner_tower);
        materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.spinner_flat);
        materialDesignSpinnerPurpose = (MaterialBetterSpinner)
                findViewById(R.id.spinner_purpose);
        mButtonCreateVistor = (Button) findViewById(R.id.create_visitor);
        mButtonCreateVistor.setOnClickListener(this);
        mTextVistorName = (TextView) findViewById(R.id.text_visitor_name);
        mTextVistorNumber = (TextView) findViewById(R.id.text_mobile);
        circularImageView = (CircularImageView) findViewById(R.id.visitor_pic);
        mTextInputLayout  = (TextInputLayout)findViewById(R.id.txtinput_vendor);
        mEditTextVendor = (EditText)findViewById(R.id.text_vendor);
        circularImageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.visitor_pic:
                showPictureDialog();
                break;
            case R.id.create_visitor:
                if (isVisitorEntryValid() || isProgressDialogShowing()) {
                    showCallingListFragment();

                }
                break;

        }

    }

    private void makeCall(String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivityForResult(callIntent, REQUEST_CALL);
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkIfAlreadyhavePermission()) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(this, new
                        String[]{Manifest.permission.CAMERA}, 1);
            }
        } else {
            dispatchTakePictureIntent();
        }

    }

    private void choosePhotoFromGallary() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkIfAlreadyhaveGalleryPermission()) {
                dispatchTakeGalleryIntent();
            } else {
                ActivityCompat.requestPermissions(this, new
                        String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
            }
        } else {
            dispatchTakeGalleryIntent();
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfAlreadyhaveGalleryPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;

            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakeGalleryIntent();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            case Constants.REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openContacts();
                }
                return;
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchTakeGalleryIntent() {
        Intent takePictureIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        ;
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_GALLERY_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4 && data != null) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                    String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    String num = "";
                    String name = "";
                    if (Integer.valueOf(hasNumber) == 1) {
                        Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (numbers.moveToNext()) {
                            num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            name = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            Contact contact = new Contact();
                            contact.setName(name);
                            contact.setNumber(num);
                            if (num.startsWith("+91")) {
                                num = num.replace("+91", "");
                                num = num.trim();
                            }
                            mTextVistorName.setText(name);
                            mTextVistorNumber.setText(num);
                            break;
                        }
                    }
                }
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                visitorImageBitmap = (Bitmap) extras.get("data");
                if (visitorImageBitmap != null) {
                    try {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        visitorImageBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
                        visitorImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        stream.flush();
                        stream.close();
                        circularImageView.setImageBitmap(visitorImageBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK && null != data) {

            Uri URI = data.getData();
            String[] FILE = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(URI,
                    FILE, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(FILE[0]);
            String ImageDecode = cursor.getString(columnIndex);
            cursor.close();
            visitorImageBitmap = BitmapFactory.decodeFile(ImageDecode);
            if (visitorImageBitmap != null) {
                try {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    visitorImageBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
                    visitorImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    stream.flush();
                    stream.close();
                    circularImageView.setImageBitmap(visitorImageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == REQUEST_CALL) {
            String template = "";
                String visitorName = mTextVistorName.getText().toString();
                String visitorNumber = mTextVistorNumber.getText().toString();
                String personToMeet = "Reception";
                if(selectedStaff != null){
                    personToMeet = selectedStaff.getName();
                }

            NavigationUtils.navigateToVisitorAction(this, REQUEST_CODE_ACTION,visitorName,visitorNumber,purpose,personToMeet);
        } else if (requestCode == REQUEST_CODE_ACTION && resultCode == RESULT_OK) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (visitorImageBitmap == null) {
                Drawable myDrawable = getResources().getDrawable(R.drawable.user_pic_def);
                visitorImageBitmap = ((BitmapDrawable) myDrawable).getBitmap();
            }
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(visitorImageBitmap, 150, 150, false);
            resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String imageEncodedData = Base64.encodeToString(byteArray, Base64.DEFAULT);
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            makeVisitorEntry(imageEncodedData);
            makeVisitorEntryOnServer();
        } else if (requestCode == REQUEST_CODE_ACTION && resultCode == RESULT_CANCELED) {
            Utils.displayToast(this, "Visitor has been denied permission");
            finish();
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkForReadPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_CONTACTS)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                            new String[]{Manifest.permission
                                                    .READ_CONTACTS},
                                            Constants.REQUEST_PERMISSIONS);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            new String[]{Manifest.permission
                                    .READ_CONTACTS},
                            Constants.REQUEST_PERMISSIONS);
                }
            }
        } else {
            //Call whatever you want
            openContacts();
        }
    }

    private void openContacts() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 4);

    }


    private void makeVisitorEntryOnServer() {
        if (Utils.isInternetAvailable(this)) {
            try {
                String url = Constants.BASE_URL + Constants.ENTRY_IN;
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(this, Request.Method.POST, url, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(Keys.NAME, mTextVistorName.getText().toString().trim());
                        params.put(Keys.PHONE, mTextVistorNumber.getText().toString().trim());
                        params.put(Keys.REASON, purpose);
                        params.put(Keys.ID,selectedStaff.getId());
                        params.put(Keys.IN,Utils.getEntryTime());
                        params.put(Keys.DESC,mEditTextVendor.getText().toString());
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        params.put(Keys.IMAGE, new DataPart(Keys.IMAGE, Utils.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(visitorImageBitmap)), "image/jpeg"));

                        return params;
                    }

                    @Override
                    public void handleReponse(BaseModel response) {
                        if (response.isStatus()) {
                            finish();
                        }
                       // Utils.displayToast(getApplicationContext(), response.getMessage());
                    }
                };
                queue.add(multipartRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void makeVisitorEntry(String imageEncodedData) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Visitor visitor = realm.createObject(Visitor.class);
        visitor.setVisitorName(mTextVistorName.getText().toString());
        visitor.setVehicleName("");
        visitor.setPurpose(purpose);
        visitor.setExited(false);
        if(selectedStaff != null){
            visitor.setFlatVisited(selectedStaff.getName());
        }else{
            visitor.setFlatVisited("Reception");
        }
        visitor.setVisitorNumber(mTextVistorNumber.getText().toString());
        visitor.setDate(Utils.getCurrentDate());
        visitor.setTime(Utils.getCurrentTime());
        visitor.setVisitorLocalImage(imageEncodedData);
        realm.commitTransaction();
        finish();
    }

    @Override
    protected void onDestroy() {
        Realm.getDefaultInstance().close();
        super.onDestroy();
    }

    private boolean isVisitorEntryValid() {
        if (TextUtils.isEmpty(mTextVistorName.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter visitor name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(mTextVistorNumber.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(TextUtils.isEmpty(purpose)){
            Toast.makeText(getApplicationContext(), "Please select purpose", Toast.LENGTH_SHORT).show();
            return false;
        } else if (purpose.equalsIgnoreCase(Constants.VENDOR)
                && TextUtils.isEmpty(mEditTextVendor.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Please enter vendor company name / job", Toast.LENGTH_SHORT).show();
            return false;
        }else if (mTextVistorNumber.getText().toString().trim().length() < 10 ) {
            Toast.makeText(getApplicationContext(), "Please enter correct  number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(purpose.equalsIgnoreCase(Constants.ADMISSION)
                 || purpose.equalsIgnoreCase(Constants.ENQUIRE)
                 || purpose.equalsIgnoreCase(Constants.VENDOR))
                && TextUtils.isEmpty(typeSelected)) {
            Toast.makeText(getApplicationContext(), "Please enter name of person to meet", Toast.LENGTH_SHORT).show();
            return false;
        } else if(!(purpose.equalsIgnoreCase(Constants.ADMISSION)
                || purpose.equalsIgnoreCase(Constants.ENQUIRE)
                || purpose.equalsIgnoreCase(Constants.VENDOR))
                && !TextUtils.isEmpty(typeSelected) && selectedStaff == null){
            Toast.makeText(getApplicationContext(), "Please select name of Person to meet", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(purpose)) {
            Toast.makeText(getApplicationContext(), "Please select Purpose for visit", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (visitorImageBitmap == null &&
                Utils.getLoggedInMode(this).equalsIgnoreCase(Constants.MODE_GUARD)) {
            Toast.makeText(getApplicationContext(), "Please take picture of visitor", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onAttendenceClicked(SchoolStaff schoolStaff) {
        if (schoolStaff != null) {
            dismissFragment();
            materialDesignSpinner1.setVisibility(View.VISIBLE);
            selectedStaff = schoolStaff;
            materialDesignSpinner1.setText(selectedStaff.getName());

        }

    }

    private void dismissFragment(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch= ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    AttendenceListFragment attendenceListFragment = (AttendenceListFragment)getSupportFragmentManager().findFragmentByTag("otp");
                    if(attendenceListFragment != null){
                        if(!TextUtils.isEmpty(query)){
                            attendenceListFragment.refreshDataOnSearch(query);
                        }else{
                            attendenceListFragment.loadDefaultData();
                        }
                    }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCallMake(String number) {
        if(!TextUtils.isEmpty(number)){
            dismissFragment();
            makeCall(number);
        }
    }
}
