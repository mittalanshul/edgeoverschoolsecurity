package school.edgeover.com.egdeoverguardsecurity.callbacks;

import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by anshul on 24/03/18.
 */

public interface OnAttendenceListClickedCallback {

    public void onAttendenceClicked(SchoolStaff schoolStaff);
}
