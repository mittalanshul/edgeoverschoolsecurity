package school.edgeover.com.egdeoverguardsecurity.Utils;

/**
 * Created by 201101101 on 10/25/2017.
 */

public class Keys {

    public static final String IDENTIFICATION = "identification";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String ADDRESS1 = "address1";
    public static final String ADDRESS2 = "address2";
    public static final String ADDRESS = "address";
    public static final String STATUS = "status";
    public static final String CREATEDAT = "createdAt";
    public static final String UPDATEDAT = "updatedAt";
    public static final String ID = "id";
    public static final String HOST = "host";
    public static final String USER = "user";
    public static final String TOKEN = "token";
    public static final String DATA = "data";
    public static final String FLAT = "flat";
    public static final String IMAGE = "image";
    public static final String IN = "in";

    public static final String AUTHORITY = "authority";
    public static final String PHONE1 = "phone1";
    public static final String PHONE2 = "phone2";
    public static final String PHONE = "phone";
    public static final String IS_JOINT_OWNER = "is_joint_owner";
    public static final String JOINT_NAME = "joint_name";
    public static final String IS_RENTED = "is_rented";
    public static final String TENANT_NAME = "tenant_name";
    public static final String TENANT_NUMBER = "tenant_number";
    public static final String RESIDENT = "resident";
    public static final String RESIDENTS = "residents";
    public static final String SUBJECT = "subject";
    public static final String SERVICE = "services";
    public static final String STAFF = "staff";
    public static final String DOMESTIC_STAFF = "domesticstaff";
    public static final String TYPE = "type";
    public static final String DESC = "description";
    public static final String TITLE = "title";
    public static final String MESSAGE = "message";
    public static final String IS_SELECTED = "isSelected";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String VEHICLE = "vehicle";
    public static final String REASON = "reason";
    public static final String ENTRY = "entry";
    public static final String EXIT = "exit";
    public static final String INTERCOM = "intercom";
    public static final String ANSWER = "answer";
    public static final String PURPOSE = "purpose";
    public static final String COMMENT = "comment";
    public static final String LOGO = "logo";
    public static final String EMERGENCY1 = "emergency1";
    public static final String EMERGENCY2 = "emergency2";
    public static final String WHEN = "when";
    public static final String EXPECTED_VISITOR_ID = "expectedId";
    public static final String HISTORY = "history";

    public static final String AMOUNT = "amount";
    public static final String PENALTYTYPE = "penaltyType";
    public static final String PENALTYINTERVAL = "penaltyInterval";
    public static final String PENALTYAMOUNT = "penaltyAmount";
    public static final String LASTDATE = "lastDate";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String PROOF = "proof";
    public static final String FATHERNAME = "fatherName";
    public static final String MOTHERNAME = "motherName";
    public static final String CLASSROOM = "classroom";
    public static final String DESIGNATION =  "designation";
    public static final String ROLE = "role";
    public static final String TEACHER = "teacher";
    public static final String CHILD = "child";

    }
