/**
 * Copyright (c) 2017 EdgeOver(A Unit of MFW Creations Pvt Ltd) .
 * This file is subject to the terms and conditions defined in file 'LICENSE.txt',
 * which is part of this source code package.All Rights Reserved
 * You may use, distribute and modify this code under the terms of the XYZ license,
 * You should have received a copy of the XYZ license with this file. If not, please write to:app.edgeover@gmail.com
 */
package school.edgeover.com.egdeoverguardsecurity.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import school.edgeover.com.egdeoverguardsecurity.R;
import school.edgeover.com.egdeoverguardsecurity.model.SchoolStaff;

/**
 * Created by 201101101 on 11/4/2017.
 */

public class StaffInfoDialog extends DialogFragment {

    private TextView textStaffName;
    private TextView textStaffNumber;
    private TextView textStaffType;
    private TextView textStaffId;
    private TextView textAttendenceDate;
    private TextView textAttendenceTime;
    private View rootView;
    private SchoolStaff staff;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.staff_info_dialog, container, false);
        getDialog().setTitle("Staff Info");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setData();
    }

    private void initViews(){
        textStaffName = (TextView)rootView.findViewById(R.id.text_staff_name);
        textStaffNumber = (TextView)rootView.findViewById(R.id.text_staff_number);
        textStaffType = (TextView)rootView.findViewById(R.id.text_staff_type);
        textStaffId = (TextView)rootView.findViewById(R.id.text_staff_id);
        textAttendenceDate = (TextView)rootView.findViewById(R.id.text_staff_attendence_date);
        textAttendenceTime = (TextView)rootView.findViewById(R.id.text_staff_attendence_time);
        rootView.findViewById(R.id.text_staff_label).setVisibility(View.GONE);
        textStaffNumber.setVisibility(View.GONE);
    }

    public void setStaffData(SchoolStaff paramStaff){
        staff = paramStaff ;
    }

    private void setData(){
        textStaffName.setText(staff.getName());
        textStaffNumber.setText(staff.getPhone());
        textStaffType.setText(staff.getRole());
        textStaffId.setText(staff.getId());
        textAttendenceDate.setText(staff.getDate() + " / " + staff.getTime());
        if(!TextUtils.isEmpty(staff.getDateExited())){
            textAttendenceTime.setText(staff.getDateExited() + " / " + staff.getTimeExited());
        }else{
            textAttendenceTime.setText("Not Exited Yet");
        }

    }
}

