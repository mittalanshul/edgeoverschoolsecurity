package school.edgeover.com.egdeoverguardsecurity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;

import school.edgeover.com.egdeoverguardsecurity.Utils.PreferenceManager;
import school.edgeover.com.egdeoverguardsecurity.Utils.Utils;
import school.edgeover.com.egdeoverguardsecurity.constants.Constants;
import school.edgeover.com.egdeoverguardsecurity.model.BaseModel;
import school.edgeover.com.egdeoverguardsecurity.parser.AllSchoolDataParser;
import school.edgeover.com.egdeoverguardsecurity.parser.BaseModelParser;

/**
 * Created by anshul on 05/04/18.
 */

public class DataUpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String url = Constants.BASE_URL + Constants.GET_ALL_GUARD_DATA;
        configureAppData(url,context);
    }

    private void configureAppData(String url, final Context context) {
        if (Utils.isInternetAvailable(context)) {
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            BaseModel baseModel = BaseModelParser.parseBaseModel(response);
                            if (baseModel.isStatus()) {
                                PreferenceManager.getInstance(context).writeToPrefs(PreferenceManager.ALL_SCHOOL_RESPONSE,response);

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return Utils.getHeaders(context);
                }

            };
            Volley.newRequestQueue(context).add(postRequest);
        }
    }
}
